'use strict';

const gulp = require('gulp'),
    rimraf = require('rimraf'),
    scss = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    prefixer = require('gulp-autoprefixer'),
    htmlmin = require('gulp-htmlmin'),
    browserSync = require('browser-sync'),
    plumber = require('gulp-plumber'),
    rigger = require('gulp-rigger'),
    reload = browserSync.reload;

var path = {
    build:{
        all:'build/',
        //html:'build/templates/'
        htaccess:'build/',
		php:'build/',
        scss:'build/assets/css/',
        css:'build/assets/css/',
        js:'build/assets/js/',
        img:'build/assets/img/',
        img_elfinder:'build/assets/img_elfinder/',
        font:'build/assets/fonts/',
        favicon:'build/assets/favicon/'
    },
    src:{
        //html:'src/templates/**/*.{html,htm,json}',
        htaccess:'src/.htaccess',
		php:'src/**/*.{php,html}',
        scss:'src/assets/scss/style.scss',
        css:'src/assets/css/*.*',
        js:'src/assets/js/main.js',
        js_plugins:'src/assets/js/plugins/*.js',
        img:'src/assets/img/**/*.{jpg,gif,png,svg,jpeg,mp4}',
        img_elfinder:'src/assets/img_elfinder/**/*.{jpg,gif,png,svg,jpeg,mp4}',
        font:'src/assets/fonts/*.*',
        favicon:'src/assets/favicon/*.*'
    },
    watch:{
		//html:'src/templates/**/*.{html,htm}',
        php:'src/**/*.php',
        scss:'src/assets/scss/**/*.scss',
        js:'src/assets/js/**/*.js',
        img:'src/assets/img/**/*.{jpg,gif,png,svg,jpeg,mp4}',
        img_elfinder:'src/assets/img_elfinder/**/*.{jpg,gif,png,svg,jpeg,mp4}'
    },
    clean:'build/'
},
config = {
    //proxy: "localhost/la_provence/build/",
    proxy: "provence.local/",
    //tunnel: true,
    notify: false
};

// чистим авгиевы конюшни
gulp.task('clean',function (done) {
    rimraf(path.clean,done);
});

// запуск webserver
gulp.task('webserver',function (done) {
    browserSync(config);
    done();
});


/* всё что относится к заданиям dev */
// работаем с php
gulp.task('dev:php',function () {
    return gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php))
        .pipe(reload({stream:true}));
});
// работаем с scss
gulp.task('dev:scss', function () {
    return gulp.src(path.src.scss,{sourcemaps: true})
        .pipe(plumber())
        .pipe(scss({
            sourcemaps:false
        }))
        .pipe(prefixer({
            cascade:false,
            remove:true
        }))
        .pipe(gulp.dest(path.build.scss,{sourcemaps:'.'}))
        .pipe(reload({stream:true}));
});
// работаем с js
gulp.task('dev:js', function () {
    return gulp.src(path.src.js,{sourcemaps: true})
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js,{sourcemaps:'.'}))
        .pipe(reload({stream:true}));
});

/* все задачи для prod */
// сжимаем html
gulp.task('prod:html',function () {
    return gulp.src(path.src.html)
        .pipe(htmlmin({
            collapseWhitespace:true
        }))
        .pipe(gulp.dest(path.build.html));
});
// переносим php
gulp.task('prod:php',function () {
    return gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php));
});
// сжимаем css, добавляем префиксы
gulp.task('prod:scss', function () {
    return gulp.src(path.src.scss)
        .pipe(scss({            
            outputStyle:"compressed",
            sourcemaps:false
        }))
        .pipe(prefixer({
            cascade:false,
            remove:true
        }))
        .pipe(gulp.dest(path.build.scss));
});
// сжимаем js
gulp.task('prod:js', function () {
    return gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
});

/* общие задачи */
// перемещаем картинки в build
gulp.task('mv:img', function () {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img));
});
// перемещаем картинки интерфейса elfinder в build
gulp.task('mv:img_elfinder', function () {
    return gulp.src(path.src.img_elfinder)
        .pipe(gulp.dest(path.build.img_elfinder));
});
// перемещаем шрифты в build
gulp.task('mv:font', function () {
    return gulp.src(path.src.font)
        .pipe(gulp.dest(path.build.font));
});
// копируем js плагины в build, сжимаем
gulp.task('js:addons', function (){
    return gulp.src(path.src.js_plugins)
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
});
// перемещаем favicon
gulp.task('mv:favicon', function () {
    return gulp.src(path.src.favicon)
        .pipe(gulp.dest(path.build.favicon));
});
// перемещаем htaccess
gulp.task('mv:htaccess', function () {
    return gulp.src(path.src.htaccess)
        .pipe(gulp.dest(path.build.htaccess));
});
// перемещаем css
gulp.task('mv:css', function () {
    return gulp.src(path.src.css)
        .pipe(gulp.dest(path.build.css));
});


// следим за изменениями в директориях
gulp.task('watch', function (done) {
    gulp.watch(path.watch.scss,gulp.series('dev:scss'));
    gulp.watch(path.build.scss,reload({stream:true}));
    //gulp.watch(path.watch.html,gulp.series('dev:html'),reload({stream:true}));
	gulp.watch(path.watch.php,gulp.series('dev:php'),reload({stream:true}));
    //gulp.watch(path.watch.php,gulp.series('dev:php'));
    gulp.watch(path.watch.js,gulp.series('dev:js'),reload({stream:true}));
    done();
});

//gulp.task('prod',gulp.series('clean',gulp.parallel('css:addons','mv:img','prod:scss','prod:js'),'webserver'));
//gulp.task('dev', gulp.series('clean', 'mv:htaccess', 'mv:css', 'dev:scss', 'mv:img', 'mv:img_elfinder', 'mv:favicon', 'mv:font', 'js:addons','dev:php','dev:js','webserver','watch'));
gulp.task('dev', gulp.series(
    'clean',
    gulp.parallel(
        'dev:scss',
        'dev:php',
        'dev:js',
        'mv:htaccess',
        'mv:css',
        'mv:img',
        'mv:img_elfinder',
        'mv:favicon',
        'mv:font',
        'js:addons'
        ),
    'webserver',
    'watch'
    ));

gulp.task('default',gulp.series('dev'));