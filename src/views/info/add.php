<!-- TODO: добавление страницы -->
<div class="container text-plain">
    <form class="mb-4" method="POST">
        <input name="new_id" type="text" class="form-control" id="bouquetId" value="<?= $pageForEdit['content_id']; ?>"
               hidden>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label for="new_header" class="form-label">Page Header</label>
                    <input name="new_header" type="text" class="form-control" id="new_header"
                           aria-describedby="new_header" value="<?= $pageForEdit['content_header']; ?>" required>
                </div>
                <div class="mb-3">
                    <label for="new_section_header" class="form-label">Section Header</label>
                    <input name="new_section_header" type="text" class="form-control" id="new_section_header"
                           aria-describedby="new_section_header" value="<?= $pageForEdit['section_header']; ?>"
                           disabled>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="section" class="form-label">Content section (1, 2, 3 ...)</label>
                    <select id="section" name="new_section" class="form-control" aria-label="section" disabled>
                        <? foreach ($contentSections as $section): ?>
                            <option value="<?= $section['section_id']; ?>"<?= ($section['section_id'] === $pageForEdit['content_section_id']) ? 'selected' : ''; ?>>
                                <?= $section['section_name']; ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="new_module" class="form-label">Location (header/footer)</label>
                    <select id="new_module" name="new_module" class="form-control" aria-label="new_module" disabled>
                        <? foreach ($contentModules as $module): ?>
                            <option value="<?= $module['content_module']; ?>"<?= ($module['content_module'] === $pageForEdit['content_module']) ? 'selected' : ''; ?>>
                                <?= $module['content_module'] == 1 ? 'header' : 'footer'; ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label for="bouquetDescription" class="form-label">Page text <span style="padding-left: 5rem;" hidden>editor: [on][off]</span></label>
                    <textarea name="content" class="form-control"
                              rows="15"><?= $pageForEdit['content_text']; ?></textarea>
                    <!--textarea name="content" class="form-control" id="ckeditor"
                              rows="15"><?= $pageForEdit['content_text']; ?></textarea-->
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>

