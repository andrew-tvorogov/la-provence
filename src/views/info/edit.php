<div class="container">
    <h5 class="cart-title mb-4 pt-4"><?= $pageForEdit['content_header']; ?></h5>
</div>

<div class="container text-plain">
    <form class="mb-4" method="POST">
        <input name="new_id" type="text" class="form-control" id="new_id" value="<?= $pageForEdit['content_id']; ?>"
               hidden>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label for="new_header" class="form-label">Page Header</label>
                    <input name="new_header" type="text" class="form-control" id="new_header"
                           aria-describedby="new_header" value="<?= $pageForEdit['content_header']; ?>" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label for="new_text" class="form-label">Page text</label>
                    <textarea name="new_text" class="form-control" id="ckeditor"
                              rows="15"><?= $pageForEdit['content_text']; ?></textarea>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>

