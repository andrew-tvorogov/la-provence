<div class="container">
    <h5 class="cart-title mb-4 pt-4">Pages</h5>
</div>

<div class="container">
    <p class="text-plain">The tables below show the data of pages automatically created by the site engine.
        Pages are divided into two categories - Header and Footer.
        You can edit pages by clicking on the edit button in the table,
        or directly on the desired page, in this case the button will be located at the bottom.
    </p>
</div>

<div class="container" style="font-size: 14px">
    <a href="<?= FULL_SITE_ROOT . 'info/add' ?>" type="button" class="btn custom-btn" hidden>Add new page</a>

    <!-- если header -->
    <h5 class="mt-4 text-plain">Header</h5>
    <table class="table mt-4 table-hover">
        <thead>
        <tr>
            <th>Header</th>
            <th class="text-center">Path</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($pages as $page): ?>
            <? if ($page['content_module'] == 1): ?>
                <tr>
                    <td>
                        <?= $page['content_header'] ?>
                    </td>
                    <td class="text-center">
                        .. / info / <?= $page['content_path'] ?>
                    </td>
                    <td class="text-right">
                        <a class="btn btn-outline-primary mb-1" style="width: 3rem"
                           href="<?= FULL_SITE_ROOT . 'info/edit/' . $page['content_id'] ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                 fill="currentColor"
                                 class="bi bi-pencil" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                            </svg>
                        </a>
                        <a class="btn btn-outline-danger mb-1" style="width: 3rem"
                           href="<?= FULL_SITE_ROOT . 'info/delete/' . $page['content_id'] ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                 fill="currentColor"
                                 class="bi bi-x" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                            </svg>
                        </a>
                    </td>
                </tr>
            <? endif; ?>
        <? endforeach; ?>
        </tbody>
    </table>

    <!-- если footer -->
    <h5 class="text-plain">Footer</h5>
    <table class="table mt-4 table-responsive-sm table-hover">
        <thead>
        <tr>
            <th>Header</th>
            <th>Path</th>
            <th>Menu header</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($pages as $page): ?>
            <? if ($page['content_module'] == 2): ?>
                <tr>
                    <td>
                        <?= $page['content_header'] ?>
                    </td>
                    <td>
                        .. / info / <?= $page['content_path'] ?>
                    </td>
                    <td>
                        <?= $page['section_header'] ?>
                    </td>
                    <td class="text-right">
                        <a class="btn btn-outline-primary mb-1" style="width: 3rem"
                           href="<?= FULL_SITE_ROOT . 'info/edit/' . $page['content_id'] ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                 fill="currentColor"
                                 class="bi bi-pencil" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                            </svg>
                        </a>
                        <a class="btn btn-outline-danger mb-1" style="width: 3rem"
                           href="<?= FULL_SITE_ROOT . 'info/delete/' . $page['content_id'] ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                 fill="currentColor"
                                 class="bi bi-x" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                            </svg>
                        </a>
                    </td>
                </tr>
            <? endif; ?>
        <? endforeach; ?>
        </tbody>
    </table>

</div>