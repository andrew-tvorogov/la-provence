<div class="container">
    <h5 class="cart-title mb-4 pt-4">Add offer</h5>
</div>

<div class="container text-plain">
    <form method="POST">
        <div class="row row-cols-1 row-cols-lg-2">
            <div class="col">
                <div class="pb-2">
                    Image
                </div>
                <div class="card p-3 mb-3 card-no-animation">
                    <div class="row row-cols-1 row-cols-lg-3 px-4">
                        <? foreach ($offerMediaFiles as $i => $mediaFile): ?>
                            <div class="col p-2">
                                <div class="card card-no-animation">
                                    <div class="card-body p-0">
                                        <div class="form_radio_btn">
                                            <input id="radio-<?= $i; ?>" type="radio" name="offer_img"
                                                   value="<?= $mediaFile; ?>">
                                            <label for="radio-<?= $i; ?>">
                                                <img class="card-img-top"
                                                     src="<?= IMG . 'offers/' . $mediaFile ?>"
                                                     alt="<?= $mediaFile ?>">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="card-footer" style="font-size: .8rem">
                                        <?= $mediaFile ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="row">
                    <div class="col">
                        <div class="mb-3">

                            <div class="form-check">
                                <input name="offer_is_active"
                                       class="form-check-input"
                                       type="checkbox"
                                       value="1"
                                       id="flexCheckDefault">
                                <label class="form-check-label" for="offer_is_active">
                                    Is active?
                                </label>
                            </div>
<!--                            <label for="offer_is_active" class="form-label">is Active?</label>-->
<!--                            <input name="offer_is_active" type="text" class="form-control" id="offer_is_active"-->
<!--                                   aria-describedby="offer_is_active"-->
<!--                                   value="0">-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>
