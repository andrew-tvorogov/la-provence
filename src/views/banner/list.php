<div class="container">
    <h5 class="cart-title mb-4 pt-4">Banners</h5>
</div>

<div class="container" style="font-size: 14px">

    <a href="<?= FULL_SITE_ROOT . 'banner/add' ?>" type="button" class="btn custom-btn">Add new
        banner</a>
    <table class="table mt-4 table-responsive-sm table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Image</th>
            <th>Main text</th>
            <th>Small text</th>
            <th style="text-align: center">Status</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($banners as $banner): ?>
            <tr>
                <td>
                    <?= $banner['banner_id'] ?>
                </td>
                <td>
                    <?= $banner['banner_img'] ?>
                </td>
                <td>
                    <?= $banner['banner_main_text'] ?>
                </td>
                <td>
                    <?= $banner['banner_small_text'] ?>
                </td>
                <td style="text-align: center">
                    <?= $banner['banner_is_active'] ? '<strong class="">enabled</strong>' :
                        '<a href="' . FULL_SITE_ROOT . 'banner/enable/' . $banner['banner_id'] . '" class="btn btn-outline-primary">enable</a>' ?>
                </td>
                <td class="text-right">
                    <a class="btn btn-outline-primary mb-1" style="width: 3rem"
                       href="<?= FULL_SITE_ROOT . 'banner/edit/' . $banner['banner_id'] ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                             fill="currentColor"
                             class="bi bi-pencil" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                  d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                        </svg>
                    </a>
                    <a class="btn btn-outline-danger mb-1" style="width: 3rem"
                       href="<?= FULL_SITE_ROOT . 'banner/delete/' . $banner['banner_id'] ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                             fill="currentColor"
                             class="bi bi-x" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                  d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                        </svg>
                    </a>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

</div>