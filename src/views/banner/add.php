<div class="container">
    <h5 class="cart-title mb-4 pt-4">Add Banner</h5>
</div>
<div class="container text-plain">
    <form method="POST">
        <div class="row row-cols-1 row-cols-lg-2">
            <div class="col">
                <div class="pb-2">Background</div>
                <div class="card p-3 mb-3 card-no-animation">
                    <div class="row row-cols-1 row-cols-lg-3 px-4">
                        <? foreach ($bannerMediaFiles as $i => $mediaFile): ?>
                            <div class="col p-2">
                                <div class="card card-no-animation">
                                    <div class="card-body p-0">
                                        <div class="form_radio_btn">
                                            <input id="radio-<?= $i; ?>" type="radio" name="banner_img"
                                                   value="<?= $mediaFile; ?>">
                                            <label for="radio-<?= $i; ?>">
                                                <img class="card-img-top"
                                                     src="<?= IMG . 'banners/' . $mediaFile ?>"
                                                     alt="<?= $mediaFile ?>">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="card-footer" style="font-size: .8rem">
                                        <?= $mediaFile ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>


                <div class="pb-2">Video Poster</div>
                <div class="card p-3 mb-3 card-no-animation">
                    <div class="row row-cols-1 row-cols-lg-3 px-4">
                        <? foreach ($bannerMediaFiles as $i => $mediaFile): ?>
                            <div class="col p-2">
                                <div class="card card-no-animation">
                                    <div class="card-body p-0">
                                        <div class="form_radio_btn">
                                            <input id="poster-radio-<?= $i; ?>" type="radio" name="banner_video_poster"
                                                   value="<?= $mediaFile; ?>">
                                            <label for="poster-radio-<?= $i; ?>">
                                                <img class="card-img-top"
                                                     src="<?= IMG . 'banners/' . $mediaFile ?>"
                                                     alt="<?= $mediaFile ?>">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="card-footer" style="font-size: .8rem">
                                        <?= $mediaFile ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>

            </div>

            <div class="col">
                <div class="pb-2">Video background</div>
                <div class="card p-3 mb-3 card-no-animation">
                    <div class="row row-cols-1 row-cols-lg-2 px-4">
                        <? foreach ($this->bannerModel->getBannerFiles('video') as $i => $mediaFile): ?>
                            <div class="col p-2">
                                <div class="card card-no-animation">
                                    <div class="card-body p-0">
                                        <div class="form_radio_btn">
                                            <input id="video-radio-<?= $i; ?>" type="radio" name="banner_video"
                                                   value="<?= $mediaFile; ?>" >
                                            <label for="video-radio-<?= $i; ?>">
                                                <video class="banner-video" style="max-height: 7rem"
                                                       src="<?= IMG . 'banners/' . $mediaFile ?>" playsinline autoplay
                                                       loop muted></video>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="card-footer" style="font-size: .8rem">
                                        <?= $mediaFile ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label for="banner_main_text" class="form-label">Main text</label>
                            <textarea name="banner_main_text" class="form-control"
                                      rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label for="banner_small_text" class="form-label">Small text</label>
                            <textarea name="banner_small_text" class="form-control"
                                      rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label for="banner_small_text_logo" class="form-label">Small text logo</label>
                            <textarea name="banner_small_text_logo" class="form-control"
                                      rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label for="banner_button_text" class="form-label">Button text</label>
                            <input name="banner_button_text" type="text" class="form-control" id="banner_button_text"
                                   aria-describedby="banner_button_text"
                                   value="">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>

