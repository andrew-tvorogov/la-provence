<?php $banner = Banner::getActiveBanner(); ?>
<? if($banner): ?>
<div class="banner">
    <div class="banner-video-wrapper m-0">
        <video class="banner-video" src="<?= IMG . 'banners/' . $banner['banner_video'] ?>" playsinline autoplay loop muted poster="<?= IMG . 'banners/' . $banner['banner_video_poster'] ?>"></video>
    </div>
    <div class="banner-bg-content" style="background: url(<?= IMG . 'banners/' . $banner['banner_img'] ?>) no-repeat center/cover;">
    </div>
    <div class="banner-overlay-content">
        <div class="container">
            <div class="row">
                <div class="col px-4 px-md-3">
                    <? if($banner['banner_small_text']): ?>
                    <div class="banner-logo">
                        <?= $banner['banner_small_text_logo'] ?>
                        <div class=" <?= $banner['banner_small_text_logo'] ? 'banner-text ml-3' : 'banner-text' ?> ">
                            <small>
                                <?= $banner['banner_small_text'] ?>
                            </small>
                        </div>
                    </div>
                    <? endif; ?>
                    <div class="h4 banner-heading font-weight-bold" style="<?= $banner['banner_small_text'] ? '' : 'margin-top: 112px' ?>">
                        <?= $banner['banner_main_text'] ?>
                    </div>
                    <? if($banner['banner_button_text']): ?>
                        <a href="<?= FULL_SITE_ROOT . 'offers'?>" class="btn custom-btn btn-banner"> <?= $banner['banner_button_text'] ?></a>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<? endif; ?>