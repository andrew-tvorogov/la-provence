<div class="container">
    <h5 class="cart-title mb-4 pt-4">Profile & Settings</h5>
</div>

<div class="container text-plain">
    <div class="row row-cols-1 row-cols-md-2 pb-4">

        <div class="col">

            <!-- profile -->
            <div class="card mb-3">
                <div class="card-header">
                    <p>My Profile</p>
                </div>
                <div class="card-body">
                    <form class="form" method="POST" action="<?= FULL_SITE_ROOT . '/cabinet/view' ?>">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <div class="mb-3">
                                        <label for="user_id" class="form-label">user id</label>
                                        <input class="form-control" type="text" name="user_id" id="user_id"
                                               value="<?= $user['user_id'] ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="mb-3">
                                        <label for="feedbackEmail" class="form-label">email</label>
                                        <input class="form-control" type="email" name="email" id="feedbackEmail"
                                               value="<?= $user['user_email'] ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="mb-3">
                                        <label for="feedbackPhone" class="form-label">phone</label>
                                        <input class="form-control" type="text" name="phone" id="feedbackPhone"
                                               value="<?= $user['user_phone'] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="mb-3">
                                        <label for="feedbackName" class="form-label">name</label>
                                        <input class="form-control" type="text" name="name" id="feedbackName"
                                               value="<?= $user['user_name'] ?>">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <button class="btn custom-btn my-4" type="submit">Update Data</button>
                    </form>
                </div>
            </div>


        </div>

        <? if (!empty($favorites)): ?>
            <div class="col">

                <div class="card mb-3">
                    <div class="card-header">
                        <p>My Favorites</p>
                    </div>
                    <div class="card-body">
                        <table class="table mt-4 table-responsive-sm">
                            <thead>
                            <tr>
                                <th>img</th>
                                <th>bouquet name</th>
                                <th>action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($favorites as $favorite): ?>
                                <tr>
                                    <td>
                                        <img style="width: 5rem"
                                             src="<?= IMG . 'bouquets/' . $favorite['bouquet_img'] ?>">
                                    </td>
                                    <td>
                                        <?= $favorite['bouquet_name'] ?>
                                    </td>
                                    <td>
                                        <button class="btn btn-outline-danger mb-1"
                                                onclick="favoritesAddRemove(<?= $favorite['bouquet_id']; ?>, <?= $_COOKIE['user']; ?>, '<?= FULL_SITE_ROOT ?>')"
                                                style="width: 3rem">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 fill="currentColor"
                                                 class="bi bi-x" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd"
                                                      d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        <? endif; ?>
    </div>
    <div class="row">

        <div class="col">
            <? if (!empty($orders)): ?>
                <div class="card mb-3">
                    <div class="card-header">
                        <p>My Orders</p>
                    </div>
                    <div class="card-body">
                        <table class="table mt-4 table-responsive-sm">
                            <thead>
                            <tr>
                                <th>img</th>
                                <th>order id</th>
                                <th>bouquet name</th>
                                <th>delivery type</th>
                                <th>created</th>
                                <th>status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($orders as $order): ?>
                                <tr>
                                    <td>
                                        <img style="width: 5rem" src="<?= IMG . 'bouquets/' . $order['bouquet_img'] ?>">
                                    </td>
                                    <td>
                                        <?= $order['order_id'] ?>
                                    </td>
                                    <td>
                                        <?= $order['bouquet_name'] ?>
                                    </td>
                                    <td>
                                        <?= $order['order_delivery_name'] ?>
                                    </td>
                                    <td>
                                        <?= $order['order_start_time'] ?>
                                    </td>
                                    <td>
                                        <?= $order['order_status_name'] ?>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <? endif; ?>
        </div>

    </div>
</div>


