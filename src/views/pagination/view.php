<div class="container pt-2">
    <div class="col">
        <? if (isset($pages)): ?>
            <ul class="nav" style="justify-content: center">
                <li class="nav-item">
                    <!--span class="nav-link disabled">Страница: </span-->
                    <!--span class="nav-link disabled">Текущая страница: <?= $current_page; ?> </span-->
                </li>
                <li class="nav-item">
                    <a class="nav-link px-0" href="<?= FULL_SITE_ROOT . $path_to_page . '1' ?>"> <<&nbsp;&nbsp; </a>
                </li>
                <? foreach ($pages as $page): ?>
                    <li class="nav-item">
                        <a class="nav-link px-0" <?= ($page == $current_page) ? "style='color: #555;'" : "style='color: #aaa'" ?>
                           href="<?= FULL_SITE_ROOT . $path_to_page . $page ?>">[ <?= $page ?> ]</a>
                    </li>
                <? endforeach; ?>
                <li>
                    <a class="nav-link px-0" href="<?= FULL_SITE_ROOT . $path_to_page . $total_pages ?>"> &nbsp;&nbsp;>> </a>
                </li>
            </ul>
        <? endif; ?>
    </div>
</div>