<div class="container mb-lg-4">
    <div style="margin-left: 0; margin-right: 0"
         class="row row-cols-1 row-cols-sm-2 row-cols-md-2 mx-sm-0 mx-md-auto">
        <div class="col p-2">
            <div class="card p-lg-4" style="border: none">
                <img style="width: 100%" class="card-img-top rounded"
                     src="<?= IMG . 'bouquets/' . $bouquet['bouquet_img'] ?>"
                     alt="Card image cap">
            </div>
        </div>
        <div class="col p-2">
            <div class="card card-no-animation text-center" style="border: none">
                <div class="card-body mt-lg-5">
                    <h5 class="card-title" style="font-size: 2rem"><?= $bouquet['bouquet_name'] ?></h5>
                    <div class="">
                        <h5 class="card-text font-weight-bold card-price"><?= $bouquet['bouquet_price'] ?>$</h5>
                    </div>
                    <div class="px-lg-5 pt-5">
                        <a class="btn custom-btn card-btn" href="<?= FULL_SITE_ROOT . 'cart' ?>"
                           onclick="addToCart(<?= $bouquet['id']; ?>)">
                            <? if (!$bouquetsCart): ?>
                                Add to cart
                            <? else: ?>
                                <? if ($this->cartModel->bouquetExistInCart($bouquet['id'], $bouquetsCart)): ?>
                                    <? foreach ($bouquetsCart as $bouquet_in_cart): ?>
                                        <? if ($bouquet['id'] === $bouquet_in_cart['bouquet_id']): ?>
                                            In cart: <?= $bouquet_in_cart['bouquet_quantity']; ?>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                <? else: ?>
                                    Add to cart
                                <? endif; ?>
                            <? endif; ?>
                        </a>
                    </div>
                    <div class="">
                        <p class="card-text text-left pt-5 px-lg-5"><?= $bouquet['bouquet_descr'] ?></p>
                    </div>
                    <? if (!$isAuthorized): ?>
                        <div class="text-right px-lg-5">
                            <? if ($bouquet['bouquet_favorites']): ?>
                                <span style="font-size: .85rem; line-height: 2rem; margin-top: 2px; color: #d8b1b1; padding-right: .25rem"><?= $bouquet['bouquet_favorites'] ?></span>
                            <? endif; ?>
                            <i style="font-size: 1.2rem; line-height: 2.5rem; margin-top: 2px; color: #d8b1b1"
                               class="fa fa-heart"
                               data-toggle="popover"
                               data-placement="auto"
                               data-content="Sign Up to add to favorites!">
                            </i>
                        </div>
                    <? endif; ?>
                    <? if ($isAuthorized): ?>
                        <div class="text-right px-lg-5">
                            <? if ($this->favoriteModel->checkIfAlreadyInFavorites($bouquet['id'], $_COOKIE['user'])): ?>
                                <?= $bouquet['bouquet_favorites'] ?>
                                <i style="font-size: 1.2rem; line-height: 40px; margin-top: 2px"
                                   class="fa fa-heart card-like"
                                   onclick="favoritesAddRemove(<?= $bouquet['id']; ?>, <?= $_COOKIE['user']; ?>, '<?= FULL_SITE_ROOT ?>')">
                                </i>
                            <? else: ?>
                                <?= $bouquet['bouquet_favorites'] ?>
                                <i style="font-size: 1.2rem; line-height: 40px; margin-top: 2px"
                                   class="fa fa-heart-o card-like"
                                   onclick="favoritesAddRemove(<?= $bouquet['id']; ?>, <?= $_COOKIE['user']; ?>, '<?= FULL_SITE_ROOT ?>')">
                                </i>
                            <? endif; ?>
                        </div>
                    <? endif; ?>
                    <? if ($isAdmin): ?>
                        <div class="text-right px-lg-5 pt-4">
                            <a class="btn btn-outline-primary mb-1" style="width: 3rem"
                               href="<?= FULL_SITE_ROOT . 'bouquet/edit/' . $bouquet['id'] ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-pencil" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                          d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                                </svg>
                            </a>
                            <a class="btn btn-outline-danger mb-1" style="width: 3rem"
                               href="<?= FULL_SITE_ROOT . 'bouquet/delete/' . $bouquet['id'] ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-x" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                          d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            </a>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>