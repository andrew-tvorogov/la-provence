<div class="container">

    <h5 class="cart-title mb-4 pt-4">Add bouquet</h5>
</div>

<div class="container text-plain">

    <form method="POST">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="bouquetName" class="form-label">Bouquet name</label>
                    <input name="name" type="text" class="form-control" id="bouquetName" aria-describedby="bouquetName" required>
                </div>
                <div class="form-group">
                    <label for="bouquetPrice" class="form-label">Price</label>
                    <input name="price" type="number" class="form-control" id="bouquetPrice"
                           aria-describedby="bouquetPrice" required>
                </div>
                <!--                <div class="mb-3">-->
                <!--                    <label for="bouquetImage" class="form-label">Image</label>-->
                <!--                    <input name="image" type="text" class="form-control" id="bouquetImage" aria-describedby="bouquetImage">-->
                <!--                </div>-->
                <div class="form-group">
                    <label for="image" class="form-label">Image</label>
                    <select id="image" name="image" class="form-control" aria-label="florist">
                        <? foreach ($images as $image): ?>
                            <option value="<?= $image; ?>"<?= ($image === 'img_temp.png') ? 'selected': ''; ?>>
                                <?= $image ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="bouquetDescription" class="form-label">Description</label>
                    <textarea name="description" class="form-control" id="bouquetDescription" rows="3"></textarea>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="florists" class="form-label">Florist</label>
                    <select name="florist_id" class="form-control" aria-label="florist">
                        <? foreach ($florists as $florist): ?>
                            <option value="<?= $florist['florist_id'] ?>" ><?= $florist['florist_name'] ?></option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="colors" class="form-label">Color</label>
                    <select name="color_id" class="form-control" aria-label="color">
                        <? foreach ($colors as $color): ?>
                            <option value="<?= $color['color_id'] ?>" ><?= $color['color_name'] ?></option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sizes" class="form-label">Size</label>
                    <select name="size_id" class="form-control" aria-label="size">
                        <? foreach ($sizes as $size): ?>
                            <option value="<?= $size['size_id'] ?>"
                                    selected><?= $size['size_name'] ?></option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="categories" class="form-label">Category</label>
                    <select name="category_id" class="form-control" aria-label="category">
                        <? foreach ($categories as $category): ?>
                            <option value="<?= $category['category_id'] ?>"
                            ><?= $category['category_name'] ?></option>
                        <? endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>
