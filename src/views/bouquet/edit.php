<div class="container">
    <h5 class="cart-title mb-4 pt-4">Edit Bouquet</h5>
</div>

<div class="container text-plain">
    <form method="POST">
        <input name="new_id" type="text" class="form-control" id="bouquetId"
               value="<?= $bouquetForEdit['bouquet_id']; ?>" hidden>


        <div class="row">
            <div class="col">
                <div class="pb-2">
                    Image
                </div>
                <div class="card p-3 mb-3 card-no-animation">
                    <div class="row row-cols-1 row-cols-lg-6 px-4">
                        <? foreach ($images as $i => $mediaFile): ?>
                            <div class="col p-2">
                                <div class="card card-no-animation">
                                    <div class="card-body p-0">
                                        <div class="form_radio_btn">
                                            <input id="radio-<?= $i; ?>" type="radio" name="new_image"
                                                   value="<?= $mediaFile; ?>" <?= ($mediaFile === $bouquetForEdit['bouquet_img']) ? 'checked' : ''; ?>>
                                            <label for="radio-<?= $i; ?>">
                                                <img class="card-img-top"
                                                     src="<?= IMG . 'bouquets/' . $mediaFile ?>"
                                                     alt="<?= $mediaFile ?>">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="card-footer" style="font-size: .8rem">
                                        <?= $mediaFile ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">

                <div class="mb-3">
                    <label for="bouquetName" class="form-label">Bouquet name</label>
                    <input name="new_name" type="text" class="form-control" id="bouquetName"
                           aria-describedby="bouquetName"
                           value="<?= $bouquetForEdit['bouquet_name']; ?>" required>
                </div>

                <div class="mb-3">
                    <label for="bouquetPrice" class="form-label">Price</label>
                    <input name="new_price" type="number" class="form-control" id="bouquetPrice"
                           aria-describedby="bouquetPrice" value="<?= $bouquetForEdit['bouquet_price']; ?>" required>
                </div>

                <div class="mb-3">
                    <label for="bouquetDescription" class="form-label">Description</label>
                    <textarea name="new_description" class="form-control" id="bouquetDescription"
                              rows="3"><?= $bouquetForEdit['bouquet_descr']; ?></textarea>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="florists" class="form-label">Florist</label>
                    <select name="new_florist_id" class="form-control" aria-label="florist">
                        <? foreach ($florists as $florist): ?>
                            <option value="<?= $florist['florist_id'] ?>"
                                <?= ($florist['florist_id'] === $bouquetForEdit['bouquet_florist_id']) ? 'selected' : ''; ?>>
                                <?= $florist['florist_name'] ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="colors" class="form-label">Color</label>
                    <select name="new_color_id" class="form-control" aria-label="color">
                        <? foreach ($colors as $color): ?>
                            <option value="<?= $color['color_id'] ?>"
                                <?= ($color['color_id'] === $bouquetForEdit['bouquet_color_id']) ? 'selected' : ''; ?>>
                                <?= $color['color_name'] ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sizes" class="form-label">Size</label>
                    <select name="new_size_id" class="form-control" aria-label="size">
                        <? foreach ($sizes as $size): ?>
                            <option value="<?= $size['size_id'] ?>"
                                <?= ($size['size_id'] === $bouquetForEdit['bouquet_size_id']) ? 'selected' : ''; ?>>
                                <?= $size['size_name'] ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="categories" class="form-label">Category</label>
                    <select name="new_category_id" class="form-control" aria-label="category">
                        <? foreach ($categories as $category): ?>
                            <option value="<?= $category['category_id'] ?>"
                                <?= ($category['category_id'] === $bouquetForEdit['bouquet_category_id']) ? 'selected' : ''; ?>>
                                <?= $category['category_name'] ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>
