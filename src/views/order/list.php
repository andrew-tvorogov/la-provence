<div class="container">
    <h5 class="cart-title mb-4 pt-4">Orders</h5>
</div>

<div class="container" style="font-size: 14px">

        <button class="btn custom-btn ml-2" hidden>Add</button>
        <table class="table mt-4 table-responsive-sm">
            <thead>
            <tr>
                <th>order id</th>
                <th>reg name</th>
                <th>reg phone</th>
                <th>delivery type</th>
                <th>delivery address</th>
                <th>start&nbsp;time</th>
                <th>sets</th>
                <th>status</th>
                <th>action</th>
            </tr>
            </thead>
            <tbody>
            <? foreach ($orders as $order): ?>
                <tr>
                    <td>
                        <?= $order['order_id'] ?>
                    </td>
                    <td>
                        <?= $order['order_reguser_name'] ?>
                    </td>
                    <td>
                        <?= $order['order_reguser_phone'] ?>
                    </td>
                    <td>
                        <?= $order['order_delivery_name'] ?>
                    </td>
                    <td>
                        <?= $order['order_delivery_address'] ?>
                    </td>
                    <td>
                        <?= $order['order_start_time'] ?>
                    </td>
                    <td>
                        <?php $sets = $this->orderModel->getOrderSets($order['order_id']); ?>
                        <ul>
                            <? foreach ($sets as $set): ?>
                                <li>
                                    id:&nbsp;<?= $set['set_bouquet_id']; ?>
                                    &nbsp;count:&nbsp;<?= $set['set_count']; ?>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </td>
                    <td>
                        <?= $order['order_status_name'] ?>
                    </td>
                    <td>
                        <!-- TODO: реализовать редактирование заказа -->
                        <a class="btn btn-outline-primary mb-1" style="width: 3rem"
                           href="<?= FULL_SITE_ROOT . 'order/edit/' . $order['order_id']; ?>" hidden>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 class="bi bi-pencil" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                            </svg>
                        </a>
                        <a class="btn btn-outline-danger mb-1" style="width: 3rem"
                           href="<?= FULL_SITE_ROOT . 'order/delete/' . $order['order_id']; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 class="bi bi-x" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                            </svg>
                        </a>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
</div>