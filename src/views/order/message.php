<div class="container">
    <div class="cart-header">
        <h5 class="cart-title mb-4 pt-4">Order details</h5>
    </div>
</div>

<div class="container mb-4">
    <!-- error -->
    <div class="card card-accordion text-center">
        <div class="card-header text-muted">
        </div>
        <div class="card-body" style="padding-top: 5rem; padding-bottom: 7rem;">
            <div class="modal-logo pt-0 mb-4">
                <svg fill="#9A7B7B" width="5.4375rem" height="4.125rem" viewBox="0 0 29 22">
                    <use href="#flower-small"/>
                </svg>
            </div>
            <h5 class="card-title mb-4">Well done!</h5>
            <p class="card-text"><!--span class="text-success"-->Your order id is: &nbsp;<strong><?= $order_id ?></strong><!--/span--></p>
            <p class="card-text pb-4"><!--span class="text-danger"-->We will call you by given telephone number <?= $user_phone ?>, to confirm order and discuss details ASAP!<!--/span--></p>
            <a href="<?= FULL_SITE_ROOT . '/bouquets/index/all' ?>" class="btn custom-btn">Choose more bouquets!</a>
        </div>
        <div class="card-footer text-muted">
        </div>
    </div>
</div>
