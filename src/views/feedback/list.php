<!-- feedback page -->

<div class="container">
    <h5 class="cart-title mb-4 pt-4">Feedback</h5>
</div>
<? if(!$isAdmin): ?>
<div class="container text-plain">
    <form class="form" method="POST" action="<?= FULL_SITE_ROOT . 'feedback/add' ?>">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="mb-3">
                        <label for="feedbackName" class="form-label">name</label>
                        <input class="form-control" type="text" name="feedbackName" id="feedbackName"
                            <? if(isset($_COOKIE['user'])): ?>
                                value="<?= $this->userModel->getUserInfo($_COOKIE['user'])['user_name']; ?>"
                            <? endif; ?>
                               required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="mb-3">
                        <label for="feedbackEmail" class="form-label">email</label>
                        <input class="form-control" type="email" name="feedbackEmail" id="feedbackEmail"
                            <? if(isset($_COOKIE['user'])): ?>
                                value="<?= $this->userModel->getUserInfo($_COOKIE['user'])['user_email']; ?>"
                            <? endif; ?>
                               required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="mb-3">
                        <label for="feedbackPhone" class="form-label">phone</label>
                        <input class="form-control" type="text" name="feedbackPhone" id="feedbackPhone"
                            <? if(isset($_COOKIE['user'])): ?>
                            value="<?= $this->userModel->getUserInfo($_COOKIE['user'])['user_phone']; ?>"
                            <? endif; ?>
                               required>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="mb-3">
                        <label for="comment" class="form-label">comment</label>
                        <textarea name="feedbackText" class="form-control" id="comment" rows="6" required></textarea>
                    </div>
                </div>
            </div>
        </div>
        <button class="mt-3 btn custom-btn mb-4" type="submit">Send</button>
    </form>
</div>
<? else: ?>
    <div class="container" style="font-size: 14px">

        <table class="table mt-4 table-responsive-sm">
            <thead>
            <tr>
                <th>feedback id</th>
                <th>user name</th>
                <th>user email</th>
                <th>user phone</th>
                <th>message</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <? foreach ($feedbacks as $feedback): ?>
                <tr>
                    <td>
                        <?= $feedback['feedback_id'] ?>
                    </td>
                    <td>
                        <?= $feedback['feedback_name'] ?>
                    </td>
                    <td>
                        <?= $feedback['feedback_email'] ?>
                    </td>
                    <td>
                        <?= $feedback['feedback_phone'] ?>
                    </td>
                    <td>
                        <?= $feedback['feedback_text'] ?>
                    </td>
                    <td>
                        <a class="btn btn-outline-danger mb-1" style="width: 3rem"
                           href="<?= FULL_SITE_ROOT . 'feedback/delete/' . $feedback['feedback_id']; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 class="bi bi-x" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                            </svg>
                        </a>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div>
<? endif; ?>