<div class="container">
    <div class="cart-header">
        <h5 class="cart-title mb-4 pt-4">Feedback</h5>
    </div>
</div>

<div class="container mb-4">
    <!-- error -->
    <div class="card card-accordion text-center card-no-animation">
        <div class="card-header text-muted">
        </div>
        <div class="card-body" style="padding-top: 5rem; padding-bottom: 7rem;">
            <div class="modal-logo pt-0 mb-4">
                <svg fill="#9A7B7B" width="5.4375rem" height="4.125rem" viewBox="0 0 29 22">
                    <use href="#flower-small"/>
                </svg>
            </div>
            <h5 class="card-title mb-4">Well done!</h5>
            <p class="card-text">Your feedback has been sent successfully!</p>
            <a href="<?= FULL_SITE_ROOT . '/bouquets/index/all' ?>" class="btn custom-btn">Go to bouquets</a>
        </div>
        <div class="card-footer text-muted">
        </div>
    </div>
</div>
