<? if ($isAdmin): ?>
    <div class="container py-3 py-md-2">
        <ul class="nav mb-2 mb-md-1 mt-md-2 mx-3 mx-md-0 nav-admin">
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'admin' ?>" class="nav-link">Overview</a>
            </li>
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'elfinder/view' ?>" class="nav-link">Files</a>
            </li>
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'orders/index/1' ?>" class="nav-link">Orders</a>
            </li>
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'feedbacks/index/1' ?>" class="nav-link">Feedback</a>
            </li>
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'users/index/1' ?>" class="nav-link">Users</a>
            </li>
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'bouquets/list/1' ?>" class="nav-link">Bouquets</a>
            </li>
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'offers/list' ?>" class="nav-link">Offers</a>
            </li>
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'banners/list' ?>" class="nav-link">Banners</a>
            </li>
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'pages/list' ?>" class="nav-link">Pages</a>
            </li>
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'employees/list' ?>" class="nav-link">Employees</a>
            </li>
            <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                <a href="<?= FULL_SITE_ROOT . 'shops/list' ?>" class="nav-link">Shop&nbsp;Setup</a>
            </li>
        </ul>
    </div>
<? endif; ?>
