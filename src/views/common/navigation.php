<nav class="container">
    <div class="row">
        <div class="col px-0 px-md-3">
            <nav class="navbar navbar-expand-sm navbar-light px-0">
                <a href="#" role="button" class="navbar-toggler mx-auto mb-2" style="border-color: white" data-toggle="collapse"
                        data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <div class="nav-hamburger">
                        <div class="nav-hamburger-line"></div>
                        <div class="nav-hamburger-line"></div>
                    </div>
                </a>
            </nav>
        </div>
    </div>
</nav>

<nav class="navbar navbar-expand-sm py-0 px-0">
    <div class="collapse navbar-collapse" id="navbarNavDropdown" style="flex-wrap: wrap">
        <div style="width: 100%">
            <div class="container">
                <ul class="nav mb-2 mb-md-3 mx-3 mx-md-0">
                    <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                        <a href="<?= FULL_SITE_ROOT ?>" class="nav-link">Home</a>
                    </li>
                    <!-- блок пунктов меню генерируется из bd -->
                    <? foreach (Menu::getHeaderMenu() as $menu_item): ?>
                        <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                            <a href="<?= FULL_SITE_ROOT . 'info/' . $menu_item['content_path'] ?>"
                               class="nav-link"><?= $menu_item['content_header'] ?></a>
                        </li>
                    <? endforeach; ?>
                    <!-- конец блока пунктов меню генерирующихся из bd -->
                    <li class="nav-item dropdown col-6 col-md-auto px-0 pb-3 pb-md-0">
                        <a href="#" class="nav-link dropdown-toggle"
                           id="navbarFlorists" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Florists
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarFlorists">
                            <a class="dropdown-item" href="<?= FULL_SITE_ROOT . 'florists/all' ?>">All</a>
                            <? foreach (Menu::getAllFloristsMenu() as $list_florist): ?>
                                <a class="dropdown-item"
                                   href='<?= FULL_SITE_ROOT . "florist/" . $list_florist["florist_id"] ?>'><?= $list_florist['florist_name'] ?></a>
                            <? endforeach; ?>
                        </div>
                    </li>
                    <? if (!$isAdmin): ?>
                        <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                            <a href="<?= FULL_SITE_ROOT . 'feedback' ?>"
                               class="nav-link">Feedback</a>
                        </li>
                    <? endif ?>
                    <? if ($isAuthorized): ?>
                        <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                            <a href="<?= FULL_SITE_ROOT . 'cabinet' ?>" class="nav-link">Profile&nbsp;&&nbsp;settings</a>
                        </li>
                    <? endif ?>
                </ul>
            </div>
        </div>
        <hr class="my-0">
        <div class="nav-admin" style="width: 100%">
            <!-- подключение панели администратора -->
            <?php include_once('views/common/navigation_admin.php'); ?>
            <!-- окончание подключения панели администратора -->
        </div>
    </div>
</nav>