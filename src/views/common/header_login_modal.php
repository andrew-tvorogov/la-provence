<div class="container">

    <!-- Modal -->
    <!-- SignIn -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="loginModalLongTitle">Log In</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-logo">
                        <svg fill="#9A7B7B" width="5.4375rem" height="4.125rem" viewBox="0 0 29 22">
                            <use href="#flower-small"/>
                        </svg>
                    </div>
                    <form class="modal-form px-4 px-md-5 pb-5" id="login_form" method="post"
                          action="<?= FULL_SITE_ROOT . 'login' ?>">
                        <div class="form-group">
                            <label for="loginEmail">Email address</label>
                            <input name="email" type="email" class="form-control" id="loginEmail"
                                   placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="loginPassword">Password</label>
                            <input name="password" type="password" class="form-control" id="loginPassword"
                                   placeholder="Password">
                            <small id="passwordHelp" class="form-text text-muted">Forgot your password?</small>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn custom-btn" form="login_form" formtarget="_self">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!-- SignUp -->
    <div class="modal fade" id="signupModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="signupModalLongTitle">Sign Up</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-logo">
                        <svg fill="#9A7B7B" width="5.4375rem" height="4.125rem" viewBox="0 0 29 22">
                            <use href="#flower-small"/>
                        </svg>
                    </div>
                    <form class="modal-form px-4 px-md-5" id="signup_form" method="post"
                          action="<?= FULL_SITE_ROOT . 'signup' ?>">
                        <div class="form-group">
                            <label id="signupEmailLable" for="signupEmail">Email address</label>
                            <input name="email" type="email" class="form-control" id="signupEmail"
                                   placeholder="Enter email" required>
                            <div class="invalid-feedback">
                                Email exists! Please, Choose another one.
                            </div>
                        </div>
                        <div class="form-group">
                            <label id="signupPasswordLable" for="signupPassword">Password</label>
                            <input name="password" type="password" class="form-control" id="signupPassword"
                                   placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label id="signupPasswordRepeatLable" for="signupPasswordRepeat">Repeat password</label>
                            <input name="password_repeat" type="password" class="form-control" id="signupPasswordRepeat"
                                   placeholder="Repeat password">
                            <div class="invalid-feedback">
                                Passwords not match! Please, check your input.
                            </div>
                        </div>
<!--                        <div id="captcha_container">-->
                            <div class="g-recaptcha py-4" data-size="normal"
                                 data-sitekey="6LdSz10aAAAAAJjU1k9gWFcI5FUsVZowDy7AtiaH"></div>
<!--                        </div>-->
                        <div class="form-check modal-check">
                            <input type="checkbox" class="form-check-input" id="signupCheckBox" name="signupCheckBox">
                            <label class="form-check-label" for="signupCheckBox"><small>I agree to the processing of
                                    personal data. <a href="#">Privacy policy</a></small></label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" id="sign_Up_Btn" class="btn custom-btn" form="signup_form" formtarget="_self"
                            disabled>Submit
                    </button>
                </div>
            </div>
        </div>
    </div>


</div>