<div class="sort">
    <nav class="container">
        <div class="row">
            <div class="col">
                <nav class="navbar navbar-expand-sm navbar-light px-0 pb-0 pb-md-2 pt-3 pt-md-2">
                    <ul class="nav mb-md-1 mt-md-2 mx-3 mx-md-0">
                        <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                        <a class="nav-item nav-link" href="<?= FULL_SITE_ROOT . 'bouquets/index/all' ?>"
                           onclick="showCards();">Show all</a>
                        </li>
                        <li class="nav-item col-6 col-md-auto px-0 pb-3 pb-md-0">
                            <a class="nav-item nav-link" href="<?= FULL_SITE_ROOT . 'bouquets/index/1' ?>"
                           onclick="showCards();">Show by page</a>
                        </li>
                        <li class="nav-item dropdown col-6 col-md-auto px-0 pb-3 pb-md-0">
                            <a href="#" class="nav-link dropdown-toggle"
                               id="sortCategory" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                Category
                            </a>
                            <div class="dropdown-menu" aria-labelledby="sortCategory" id="sort">
                                <a class="dropdown-item" href="#sort" onclick="showCards();">All</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('category', 0);">New Staff</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('category', 1);">Large Bouquets
                                    & Baskets</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('category', 2);">Common
                                    Collection</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('category', 3);">Wedding
                                    Bouquets</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('category', 4);">Hat Boxes</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('category', 5);">Bouquets</a>
                                <a class="dropdown-item" href="#sort"
                                   onclick="sortCards('category', 6);">Compositions</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('category', 7);">New Year
                                    Collections</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown col-6 col-md-auto px-0 pb-3 pb-md-0">
                            <a href="#" class="nav-link dropdown-toggle"
                               id="sortColor" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                Color
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sortColor">
                                <a class="dropdown-item" href="#sort" onclick="showCards();">
                                    <div class="sort-color-circle"></div>
                                    All</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('color', 1);">
                                    <div class="sort-color-circle sort-white"></div>
                                    White</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('color', 2);">
                                    <div class="sort-color-circle sort-blue"></div>
                                    Light Blue</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('color', 3);">
                                    <div class="sort-color-circle sort-orange"></div>
                                    Light Orange</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('color', 4);">
                                    <div class="sort-color-circle sort-green"></div>
                                    Pale Green</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('color', 5);">
                                    <div class="sort-color-circle sort-red"></div>
                                    Red</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('color', 6);">
                                    <div class="sort-color-circle sort-pink"></div>
                                    Pink</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('color', 7);">
                                    <div class="sort-color-circle sort-rose"></div>
                                    Rose</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('color', 8);">
                                    <div class="sort-color-circle sort-purple"></div>
                                    Purple</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('color', 0);">
                                    <div class="sort-color-circle"></div>
                                    Other</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown col-6 col-md-auto px-0 pb-3 pb-md-0">
                            <a href="#" class="nav-link dropdown-toggle"
                               id="sortSize" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                Size
                            </a>
                            <div class="dropdown-menu" aria-labelledby="sortSize">
                                <a class="dropdown-item" href="#sort" onclick="showCards();">All</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('size', 0);">Small</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('size', 1);">Medium</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('size', 2);">Large</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('size', 3);">Extra Large</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown col-6 col-md-auto px-0 pb-3 pb-md-0">
                            <a href="#" class="nav-link dropdown-toggle"
                               id="sortPrice" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                Price
                            </a>
                            <div class="dropdown-menu" aria-labelledby="sortPrice">
                                <a class="dropdown-item" href="#sort" onclick="showCards();">All</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('price', 0);">up to 30$</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('price', 1);">30$ - 70$</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('price', 2);">70$ - 150$</a>
                                <a class="dropdown-item" href="#sort" onclick="sortCards('price', 3);">150$ - 300$</a>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </nav>
</div>