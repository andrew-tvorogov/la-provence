<div class="container" style="margin-top: 1.2rem">
    <header class="navbar navbar-expand flex-column flex-md-row bd-navbar" style="padding: .5rem 0">
        <a class="navbar-brand mr-0 mr-md-3" href="<?= FULL_SITE_ROOT ?>">
            <svg class="main-header-logo" width="50" height="38" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M24.7283 36.6857C39.6031 28.7661 32.2531 10.9875 24.7993 0.888947C18.5955 10.282 9.39342 27.745 24.7283 36.6857Z"
                      fill="#C29B9B"/>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M25.0712 36.1966C24.9581 36.2591 24.8437 36.321 24.728 36.3823C24.6313 36.3262 24.5357 36.2699 24.441 36.2132C16.5687 28.0914 19.4643 19.1933 24.7481 11.9355C30.0282 19.1884 32.9235 28.0793 25.0712 36.1966ZM24.7481 36.4643C24.7643 36.4681 24.7805 36.4719 24.7967 36.4757C24.8066 36.4657 24.8166 36.4558 24.8265 36.4458C24.8004 36.452 24.7743 36.4582 24.7481 36.4643ZM24.6697 36.4458C24.675 36.4511 24.6803 36.4564 24.6856 36.4617L24.6995 36.4757C24.7157 36.4719 24.7319 36.4681 24.7481 36.4643C24.7219 36.4582 24.6958 36.452 24.6697 36.4458Z"
                      fill="#9A7B7B"/>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M28.8104 7.10661C27.3966 8.62244 26.0051 10.2642 24.7486 11.9992C23.5539 10.3494 22.2371 8.78396 20.895 7.3309C22.1673 4.90814 23.5447 2.67821 24.7994 0.778412C26.1877 2.65919 27.5723 4.80636 28.8104 7.10661ZM24.827 36.639C41.0983 32.7433 38.5697 13.7408 33.976 2.09494C32.3643 3.52848 30.57 5.22002 28.8104 7.10661C34.1684 17.0611 36.7828 29.8827 25.0717 36.3885C24.9913 36.4721 24.9097 36.5556 24.827 36.639ZM24.7486 36.6577C24.7648 36.6615 24.781 36.6653 24.7972 36.669C24.8072 36.659 24.8171 36.649 24.827 36.639C24.8009 36.6453 24.7748 36.6515 24.7486 36.6577ZM24.6702 36.639C24.593 36.5611 24.5168 36.4832 24.4416 36.4051C12.5487 29.2447 15.9167 16.8114 20.895 7.3309C19.0691 5.35396 17.1965 3.58506 15.5212 2.09494C10.9276 13.7408 8.39896 32.7433 24.6702 36.639ZM24.6702 36.639C24.6755 36.6444 24.6808 36.6497 24.6861 36.6551L24.7 36.669C24.7163 36.6653 24.7324 36.6615 24.7486 36.6577C24.7224 36.6515 24.6963 36.6453 24.6702 36.639Z"
                      fill="#E2B5B5"/>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M25.0212 36.7425C34.6023 38.2745 37.2204 34.8538 39.6757 31.6459C39.8347 31.4381 39.9931 31.2312 40.1526 31.0266C40.9125 30.0519 41.5168 29.2203 42.0265 28.5188C43.518 26.466 44.1994 25.5282 45.6014 25.3832C46.7643 25.2629 48.1195 25.8405 49.2868 26.5403C49.2533 26.5112 49.2188 26.4811 49.1833 26.4501C48.1598 25.5567 46.2973 23.9311 44.1743 24.1507C42.2932 24.3453 41.8391 24.5399 38.012 28.7563C37.5871 29.2243 37.1771 29.6909 36.7738 30.15C33.6062 33.755 30.8491 36.8929 24.5197 36.6052C24.5919 36.6249 24.6664 36.6453 24.7429 36.6663C24.8328 36.691 24.9256 36.7164 25.0212 36.7425Z"
                      fill="#E2B5B5"/>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M42.344 30.5906C41.3802 31.6787 40.3147 32.8816 39.0129 34.0711C33.7135 38.9133 27.8318 37.5005 24.7791 36.6734C34.4111 38.2053 37.0432 34.7847 39.5116 31.5768C39.6715 31.369 39.8307 31.1621 39.9911 30.9575C40.7551 29.9828 41.3626 29.1511 41.8749 28.4497C43.3745 26.3969 44.0595 25.4591 45.469 25.3141C46.6381 25.1938 48.0006 25.7714 49.1741 26.4712C49.1782 26.4747 49.1822 26.4782 49.1862 26.4817C46.3058 26.1178 44.6045 28.0385 42.344 30.5906Z"
                      fill="#9A7B7B"/>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M24.2656 36.7425C14.6846 38.2745 12.0665 34.8538 9.6112 31.6459C9.45215 31.4381 9.29379 31.2312 9.13427 31.0266C8.37433 30.0519 7.77007 29.2202 7.26041 28.5188C5.76887 26.466 5.08749 25.5282 3.68544 25.3832C2.52257 25.2629 1.16735 25.8405 0.000102997 26.5403C0.0335598 26.5112 0.0680485 26.4811 0.103548 26.4501C1.12706 25.5567 2.98957 23.9311 5.11252 24.1507C6.99366 24.3453 7.44773 24.5399 11.2749 28.7563C11.6997 29.2243 12.1097 29.6909 12.5131 30.15C15.6806 33.755 18.4378 36.8929 24.7672 36.6052C24.6949 36.6249 24.6205 36.6453 24.5439 36.6663C24.454 36.691 24.3612 36.7164 24.2656 36.7425Z"
                      fill="#E2B5B5"/>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M6.9429 30.5906C7.90669 31.6787 8.97214 32.8816 10.274 34.0711C15.5734 38.9133 21.455 37.5005 24.5078 36.6734C14.8757 38.2053 12.2437 34.7847 9.77527 31.5768C9.61537 31.369 9.45617 31.1621 9.29579 30.9575C8.5318 29.9828 7.92431 29.1512 7.41192 28.4497C5.91241 26.3969 5.22739 25.4591 3.81784 25.3141C2.64874 25.1938 1.28623 25.7714 0.112728 26.4712C0.108692 26.4747 0.10467 26.4782 0.100662 26.4817C2.98106 26.1178 4.68234 28.0385 6.9429 30.5906Z"
                      fill="#9A7B7B"/>
            </svg>
        </a>
        <a class="navbar-brand mr-0 mr-md-2" href="/" aria-label="" hidden>
            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" class="d-block" viewBox="0 0 612 612"
                 role="img" focusable="false"><title><?= Shop::getActiveShop()['shop_name']; ?></title>
                <path fill="currentColor"
                      d="M510 8a94.3 94.3 0 0 1 94 94v408a94.3 94.3 0 0 1-94 94H102a94.3 94.3 0 0 1-94-94V102a94.3 94.3 0 0 1 94-94h408m0-8H102C45.9 0 0 45.9 0 102v408c0 56.1 45.9 102 102 102h408c56.1 0 102-45.9 102-102V102C612 45.9 566.1 0 510 0z"></path>
                <path fill="currentColor"
                      d="M196.77 471.5V154.43h124.15c54.27 0 91 31.64 91 79.1 0 33-24.17 63.72-54.71 69.21v1.76c43.07 5.49 70.75 35.82 70.75 78 0 55.81-40 89-107.45 89zm39.55-180.4h63.28c46.8 0 72.29-18.68 72.29-53 0-31.42-21.53-48.78-60-48.78h-75.57zm78.22 145.46c47.68 0 72.73-19.34 72.73-56s-25.93-55.37-76.46-55.37h-74.49v111.4z"></path>
            </svg>
        </a>
        <div class="navbar-nav-scroll">
            <!-- имя магазина -->
            <a class="nav-link main-header-logo-text" style="padding-right: 0" href="<?= FULL_SITE_ROOT ?>"><?= SHOP::getActiveShop()['shop_name']; ?></a>
        </div>
        <ul class="navbar-nav ml-md-auto">
            <li class="nav-item">
                <a class="nav-link pl-2 pr-1 mx-1 py-3 my-n2 header-text" href="#" rel="noopener"
                   aria-label="telephone"><?= Shop::getActiveShop()['shop_phone']; ?></a>
            </li>
            <li class="nav-item dropdown">
                <? if(isset($_COOKIE['user_name'])): ?>
                <a class="nav-link px-1 mx-1 py-3 my-n2 header-text" href="<?= FULL_SITE_ROOT . 'logout' ?>"><span style="color: #9A7B7B; font-size: .75rem"><?= $_COOKIE['user_name']; ?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="1.4rem" height="1.4rem" fill="currentColor" class="bi bi-box-arrow-right" viewBox="0 1 16 16">
                        <path fill-rule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                        <path fill-rule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                    </svg>
                </a>
                <? else: ?>
                <a class="nav-link px-1 mx-1 py-3 my-n2 header-text" href="#" id="bd-versions"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <svg width="1.4rem" height="1.4rem" viewBox="0 1 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                    </svg>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
                    <input type='button' data-toggle="modal" data-target="#loginModal" class="dropdown-item" value="Log In">
                    <div class="dropdown-divider"></div>
                    <input type='button' data-toggle="modal" data-target="#signupModal" class="dropdown-item" value="Sign Up">
                </div>
                <? endif; ?>
            </li>
            <? if(!$isAdmin): ?>
            <li class="nav-item">
                <a class="nav-link px-1 mx-1 py-3 my-n2 header-text" href="<?= FULL_SITE_ROOT . 'cart' ?>" rel="noopener" aria-label="Slack">
<!--                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>-->
                    <svg width="1.3rem" height="1.4rem" viewBox="0 2 16 16" class="bi bi-cart2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l1.25 5h8.22l1.25-5H3.14zM5 13a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
                    </svg>
                    <span id="cart_items_counter_view"
                          style="
                          display: inline-block;
                          width: 16px;
                          height: 16px;
                          text-align: center;
                          color: #9A7B7B;
                          font-size: .75rem;
                          border: 1px solid #C29B9B;
                          border-radius: 50%;
                          line-height: 14px;">
                    </span>
                </a>
            </li>
            <? endif; ?>
        </ul>
    </header>
</div>



