<div class="container pt-2">
    <div class="col">
        <? if (isset($pages)): ?>
            <ul class="nav" style="justify-content: center">
                <li class="nav-item">
                    <!--span class="nav-link disabled">Страница: </span-->
                    <!--span class="nav-link disabled">Текущая страница: <?= $current_page; ?> </span-->
                </li>
                <? foreach ($pages as $page): ?>
                    <li class="nav-item">
                        <a class="nav-link px-0" <?= ($page == $current_page) ? "style='color: #555;'" : "style='color: #aaa'" ?>
                           href="<?= FULL_SITE_ROOT . 'bouquets/index/' . $page ?>">[ <?= $page ?> ]</a>
                    </li>
                <? endforeach; ?>
            </ul>
        <? endif; ?>
    </div>
</div>