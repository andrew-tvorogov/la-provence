<div class="container">
    <h5 class="cart-title mb-4 pt-4">Overview</h5>
</div>

<div class="container">
    <div class="row row-cols-1 row-cols-md-4">
        <div class="col mb-3">
            <div class="card text-left">
                <div class="card-header small">
                    TOTAL ORDERS
                </div>
                <div class="card-body">
                    <div class="card-title d-flex justify-content-between mb-0">
                        <h4>
                            <i class="fa fa-credit-card text-secondary" aria-hidden="true"></i>
                        </h4>
                        <h4>
                            <strong class="text-center"><?= $orders_count; ?></strong>
                        </h4>
                    </div>
                </div>
                <div class="card-footer small">
                    <a href="<?= FULL_SITE_ROOT . 'orders/index/1' ?>">View more...</a>
                </div>
            </div>
        </div>
        <div class="col mb-3">
            <div class="card text-left">
                <div class="card-header small">
                    TOTAL USERS
                </div>
                <div class="card-body">
                    <div class="card-title d-flex justify-content-between mb-0">
                        <h4>
                            <i class="fa fa-user text-secondary" aria-hidden="true"></i>
                        </h4>
                        <h4>
                            <strong class="text-center"><?= $users_count; ?></strong>
                        </h4>
                    </div>
                </div>
                <div class="card-footer small">
                    <a href="<?= FULL_SITE_ROOT . 'users/index/all' ?>">View more...</a>
                </div>
            </div>
        </div>
        <div class="col mb-3">
            <div class="card text-left">
                <div class="card-header small">
                    TOTAL BOUQUETS
                </div>
                <div class="card-body">
                    <div class="card-title d-flex justify-content-between mb-0">
                        <h4>
                            <i class="fa fa-yelp text-secondary" aria-hidden="true"></i>
                        </h4>
                        <h4>
                            <strong class="text-center"><?= $bouquets_count; ?></strong>
                        </h4>
                    </div>
                </div>
                <div class="card-footer small">
                    <a href="<?= FULL_SITE_ROOT . 'bouquets/list/0' ?>">View more...</a>
                </div>
            </div>
        </div>
        <div class="col mb-3">
            <div class="card text-left">
                <div class="card-header small">
                    TOTAL IN FAVORITES
                </div>
                <div class="card-body">
                    <div class="card-title d-flex justify-content-between mb-0">
                        <h4>
                            <i class="fa fa-heart-o text-secondary" aria-hidden="true"></i>
                        </h4>
                        <h4>
                            <strong class="text-center"><?= $favorites_count; ?></strong>
                        </h4>
                    </div>
                </div>
                <div class="card-footer small">
                    <a href="<?= FULL_SITE_ROOT . 'bouquets/index/all' ?>">View more...</a>
                </div>
            </div>
        </div>
    </div>
</div>
