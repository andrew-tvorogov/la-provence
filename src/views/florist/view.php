<div class="container">
    <h5 class="cart-title mb-4 pt-4"><?= $selectedFlorist['florist_name'] ?></h5>
</div>

<div class="container">
    <p class="text-plain">These bouquets were designed by <?= $selectedFlorist['florist_name'] ?>. If you have any questions, please leave us a <a href="<?= FULL_SITE_ROOT . 'feedback' ?>">comment</a>.</p>
    <table class="table mt-4 table-responsive">
<!--        <thead>-->
<!--        <tr>-->
<!--            <th class="text-plain">image</th>-->
<!--            <th>name</th>-->
<!--            <th>description</th>-->
<!--        </tr>-->
<!--        </thead>-->
        <tbody>
        <? foreach($floristsBouquets as $selectedFloristsBouquet): ?>
            <tr>
                <td>
                    <img style="width: 120px" src="<?= IMG . "bouquets/" . $selectedFloristsBouquet['bouquet_img'] ?>"/>
                </td>
                <td>
                    <?= $selectedFloristsBouquet['bouquet_name'] ?>
                </td>
                <td>
                    <?= $selectedFloristsBouquet['bouquet_descr'] ?>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
</div>