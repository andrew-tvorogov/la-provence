<div class="container">
    <h5 class="cart-title mb-4 pt-4">Florist Edit</h5>
</div>

<div class="container text-plain">
    <form method="POST">

        <div class="row">
            <div class="col">
                <div class="form-group" hidden>
                    <div class="mb-3">
                        <label for="florist_id" class="form-label">florist id</label>
                        <input class="form-control" type="number" name="florist_id" id="florist_id"
                               value="<?= $florist_for_edit['florist_id'] ?>" readonly>
                    </div>
                </div>


                <div class="pb-2">
                    Image
                </div>
                <div class="card p-3 mb-3 card-no-animation">
                    <div class="row row-cols-1 row-cols-lg-3 px-4">
                        <? foreach ($floristsMediaFiles as $i => $mediaFile): ?>
                            <div class="col p-2">
                                <div class="card card-no-animation">
                                    <div class="card-body p-0">
                                        <div class="form_radio_btn">
                                            <input id="radio-<?= $i; ?>" type="radio" name="florist_img"
                                                   value="<?= $mediaFile; ?>" <?= ($mediaFile === $florist_for_edit['florist_img']) ? 'checked' : ''; ?>>
                                            <label for="radio-<?= $i; ?>">
                                                <img class="card-img-top"
                                                     src="<?= IMG . 'florists/' . $mediaFile ?>"
                                                     alt="<?= $mediaFile ?>">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="card-footer" style="font-size: .8rem">
                                        <?= $mediaFile ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>

            </div>
            <div class="col">
                <div class="form-group">
                    <div class="mb-3">
                        <label for="florist_name" class="form-label">name</label>
                        <input class="form-control" type="text" name="florist_name" id="florist_name"
                               value="<?= $florist_for_edit['florist_name'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="mb-3">
                        <label for="florist_surname" class="form-label">surname</label>
                        <input class="form-control" type="text" name="florist_surname" id="florist_surname"
                               value="<?= $florist_for_edit['florist_surname'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="florist_phone" class="form-label">phone</label>
                    <input class="form-control" type="text" name="florist_phone" id="florist_phone"
                           value="<?= $florist_for_edit['florist_phone'] ?>">
                </div>
                <div class="mb-3">
                    <label for="florist_descr" class="form-label">description</label>
                    <textarea name="florist_descr" class="form-control"
                              rows="4"><?= $florist_for_edit['florist_descr']; ?></textarea>
                </div>
            </div>

        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>