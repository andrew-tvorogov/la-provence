<div class="container">
    <h5 class="cart-title mb-4 pt-4">Florists</h5>
</div>

<div class="container cards pt-0 mb-4" style="font-size: 14px">
    <div style="margin-left: 0; margin-right: 0"
         class="row row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-lg-3 row-cols-xl-4 mx-sm-0 mx-md-auto">
        <? foreach($florists as $florist): ?>
            <div class="col p-2">
                <div class="card">
                    <a href="<?= FULL_SITE_ROOT . "florist/" . $florist["florist_id"] ?>">
                    <img style="width: 100%" class="card-img-top" src="<?= IMG . 'florists/' . $florist['florist_img'] ?>"
                         alt="Card image cap">
                    </a>
                    <div class="card-body pb-3">
                        <h5 class="card-title"><?= $florist['florist_name'] ?></h5>
                        <div class="card-descr">
                            <p class="card-text"><?= $florist['florist_descr'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>