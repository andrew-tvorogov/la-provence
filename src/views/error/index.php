<?php
$errcode = array(
    '1' => 'Can not add your message. Please, try later!',
    '2' => 'Bouquet with this ID does not exist',
    '3' => 'Florist with this ID does not exist',
    '4' => 'Banner with this ID does not exist',
	'5' => 'User with this email/password does not exist',
    '6' => 'Please, fill the CAPTCHA checkbox!',
    '7' => 'Robot detected!',
    '404' => 'Page does not exist!'
);
$title = "Error!";
?>

<?php include_once('views/common/header.php'); ?>

<div class="container mt-4 mb-5">
    <div class="card text-center">
        <div class="card-header">
            Error
        </div>
        <div class="card-body" style="padding-top: 5rem; padding-bottom: 7rem;">
            <div class="modal-logo pt-0 mb-4">
                <svg fill="#9A7B7B" width="5.4375rem" height="4.125rem" viewBox="0 0 29 22">
                    <use href="#flower-small"/>
                </svg>
            </div>
            <!--h5 class="card-title">Something went wrong!</h5-->
            <p class="card-text pb-4"><span class="text-danger"><?= $errcode[$error]; ?> </span></p>
            <a href="<?= FULL_SITE_ROOT ?>" class="btn custom-btn">To main page</a>
        </div>
        <div class="card-footer text-muted">
            Error code: <?= $error; ?>
        </div>
    </div>
</div>

<?php include_once('views/common/footer.php'); ?>