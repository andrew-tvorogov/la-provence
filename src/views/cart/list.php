<div class="container">
    <div class="cart-header">
        <h5 class="cart-title">Your cart</h5>
    </div>
    <? if ($bouquetsCart): ?>
    <div style="border-bottom: 1px solid #dedede;"></div>
    <? endif; ?>
</div>

<div class="container">
    <!-- cart item -->
    <? if ($bouquetsCart): ?>
    <? foreach($bouquetsCart as $orderBouquet): ?>
    <div class="cart-item" data-id="<?= $orderBouquet['bouquet_id'] ?>">
        <div class="row py-2">
            <div class="col-6 col-md-2">
                <a href="#">
                    <img src="<?= IMG . 'bouquets/' . $orderBouquet['bouquet_img'] ?>" style="width: 9rem"
                         alt=" description of item">
                </a>
            </div>
            <div class="col-6 col-md-10 px-0">
                <div class="col-sm" style="height: 100%">
                    <div class="row align-items-center" style="height: 100%">
                        <div class="col-sm-4 text-right">
                            <h5 class="cart-item-title"><?= $orderBouquet['bouquet_name'] ?></h5>
                        </div>
                        <div class="col-sm-4 text-right">
                            <div style="margin-bottom: .75rem">
                                <button onclick="removeFromCart(<?= $orderBouquet['bouquet_id']; ?>)" class="card-count_btn" style="display: inline-block">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <div class="card-count_num"
                                     style="display: inline-block"><?= $orderBouquet['bouquet_quantity'] ?></div>
                                <button onclick="addToCart(<?= $orderBouquet['bouquet_id']; ?>)" class="card-count_btn" style="display: inline-block">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button onclick="deleteFromCart(<?= $orderBouquet['bouquet_id']; ?>)" class="card-count_btn" style="font-size: 20px; padding-left: 20px; display: inline-block; color: red;">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-4 font-weight-bold card-price text-right">
                            <span><?= $orderBouquet['bouquet_price'] * $orderBouquet['bouquet_quantity'] ?>$</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? endforeach; ?>
    <? else: ?>
    <!-- error -->
    <div class="card card-accordion text-center mb-4">
        <div class="card-header text-muted">
        </div>
        <div class="card-body" style="padding-top: 5rem; padding-bottom: 7rem;">
            <!--                <h5 class="card-title"></h5>-->
            <div class="modal-logo pt-0 mb-4">
                <svg fill="#9A7B7B" width="5.4375rem" height="4.125rem" viewBox="0 0 29 22">
                    <use href="#flower-small"/>
                </svg>
            </div>
            <p class="card-text"><!--span class="text-danger"-->Your cart is empty!<br>Please, choose a bouquet first<!--/span--></p>
            <a href="<?= FULL_SITE_ROOT . '/bouquets/index/all' ?>" class="btn custom-btn">Go to bouquets</a>
        </div>
        <div class="card-footer text-muted">
        </div>
    </div>
    <? endif; ?>
</div>



<? if ($bouquetsCart): ?>
<!-- форма для ввода пользовательских данных -->
<div class="container mt-4">
    <div class="row" style="padding-bottom: 1rem">
        <div class="col-md-7 col-sm-6 text-plain">
            <form class="form" id="order_create" method="POST" action="<?= FULL_SITE_ROOT . 'order/add'  ?>">

                <div class="row">
                    <div class="col">
                        <div class="form-group mb-4">
                            <div class="mb-3">
                                <label for="card_message" class="form-label">card message to recipient</label>
                                <textarea name="card_message" class="cart-textarea-recipient-message form-control" id="card_message" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                                <label for="userName" class="form-label">your name</label>
                                <input class="form-control" type="text" name="user_name" id="userName"
                                <? if(isset($_COOKIE['user'])): ?>
                                       value="<?= $_COOKIE['user_name']; ?>
                                <? endif; ?>" required>
                                <input type="text" name="user_id" id="userId"
                                <? if(isset($_COOKIE['user'])): ?>
                                value="<?= $_COOKIE['user']; ?>
                                <? endif; ?>" hidden>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                                <label for="userPhone" class="form-label">your phone</label>
                                <input class="form-control" type="text" name="user_phone" id="userPhone"
                                <? if(isset($_COOKIE['user'])): ?>
                                value="<?= $this->userModel->getUserInfo($_COOKIE['user'])['user_phone']; ?>
                                <? endif; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="deliveryType" class="form-label">delivery type</label>
                            <select name="delivery_type_id" class="form-control" aria-label="deliveryType" id="deliveryType">
                                <? foreach ($deliveries as $delivery): ?>
                                    <option value="<?= $delivery['delivery_id'] ?>" ><?= $delivery['delivery_name'] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                                <label for="deliveryAddress" class="form-label">delivery address</label>
                                <input class="form-control" type="text" name="delivery_address" id="deliveryAddress"
                                    <? if(isset($_COOKIE['user'])): ?>
                                       value="<?= $this->userModel->getUserInfo($_COOKIE['user'])['address_city']; ?><? endif; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                                <label for="userPhone" class="form-label">delivery date</label>
                                <input class="form-control" type="datetime-local" name="delivery_date" id="deliveryDate" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                                <label for="comment" class="form-label">order comments</label>
                                <textarea name="comment" class="form-control" id="comment" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-5 col-sm-6 mt-4 text-plain text-right">
<!--                    <span class="mr-auto">User ID: <?= $userId; ?></span><br><br>-->
                    <span class="mr-auto">Total: </span>
                    <span class="font-weight-bold card-price"><?= $total; ?>$</span><br>

                <button class="btn custom-btn ml-2 my-3" form="order_create">Make Order</button>

        </div>
    </div>

</div>
<? endif; ?>