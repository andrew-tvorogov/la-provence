<div class="container">
    <h5 class="cart-title mb-4 pt-4">User Edit</h5>
</div>

<div class="container text-plain">
    <form method="POST">
        <input name="new_id" type="text" class="form-control" id="bouquetId" value="<?= $userForEdit['user_id']; ?>"
               hidden>
        <div class="row">
            <div class="col">

                <div class="form-group">
                    <div class="mb-3">
                        <label for="user_id" class="form-label">user id</label>
                        <input class="form-control" type="text" name="user_id" id="user_id"
                               value="<?= $userForEdit['user_id'] ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <div class="mb-3">
                        <label for="user_email" class="form-label">email</label>
                        <input class="form-control" type="email" name="user_email" id="user_email"
                               value="<?= $userForEdit['user_email'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="mb-3">
                        <label for="user_phone" class="form-label">phone</label>
                        <input class="form-control" type="text" name="user_phone" id="user_phone"
                               value="<?= $userForEdit['user_phone'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="mb-3">
                        <label for="user_name" class="form-label">name</label>
                        <input class="form-control" type="text" name="user_name" id="user_name"
                               value="<?= $userForEdit['user_name'] ?>">
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="user_is_deleted" class="form-label">is deleted</label>
                    <select name="user_is_deleted" class="form-control" aria-label="user_is_deleted">
                        <option value="0"
                            <?= ($userForEdit['user_is_deleted'] == 0) ? 'selected' : ''; ?>>
                            no
                        </option>
                        <option value="1"
                            <?= ($userForEdit['user_is_deleted'] == 1) ? 'selected' : ''; ?>>
                            yes
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="user_is_admin" class="form-label">is admin</label>
                    <select name="user_is_admin" class="form-control" aria-label="user_is_admin">
                        <option value="0"
                            <?= ($userForEdit['user_is_admin'] == 0) ? 'selected' : ''; ?>>
                            no
                        </option>
                        <option value="1"
                            <?= ($userForEdit['user_is_admin'] == 1) ? 'selected' : ''; ?>>
                            yes
                        </option>
                    </select>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>