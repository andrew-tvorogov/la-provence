<div class="container">
    <h5 class="cart-title mb-4 pt-4">Edit shop</h5>
</div>
<div class="container text-plain">
    <form method="POST">
        <input name="shop_id" type="text" class="form-control" id="shop_id"
               value="<?= $shop_for_edit['shop_id']; ?>" hidden>
        <div class="row row-cols-1 row-cols-lg-2">


            <div class="col">
                <div class="mb-3">
                    <label for="shop_name" class="form-label">Name</label>
                    <input name="shop_name" type="text" class="form-control" id="shop_name"
                           aria-describedby="shop_name"
                           value="<?= $shop_for_edit['shop_name']; ?>">
                </div>
            </div>

            <div class="col">
                <div class="mb-3">
                    <label for="shop_email" class="form-label">Email</label>
                    <input name="shop_email" type="email" class="form-control" id="shop_email"
                           aria-describedby="shop_email"
                           value="<?= $shop_for_edit['shop_email']; ?>">
                </div>
            </div>

            <div class="col">
                <div class="mb-3">
                    <label for="shop_phone" class="form-label">Phone</label>
                    <input name="shop_phone" type="phone" class="form-control" id="shop_phone"
                           aria-describedby="shop_phone"
                           value="<?= $shop_for_edit['shop_phone']; ?>">
                </div>
            </div>

            <? if ($shop_for_edit['shop_is_active']): ?>
                <div class="col" hidden>
                    <input name="shop_is_active" type="text" class="form-control" id="shop_is_active"
                           aria-describedby="shop_phone"
                           value="<?= $shop_for_edit['shop_is_active']; ?>">
                </div>
            <? else: ?>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <div class="form-check">
                                    <input name="shop_is_active"
                                           class="form-check-input"
                                           type="checkbox"
                                           value="<?= $shop_for_edit['shop_is_active']; ?>"
                                           id="flexCheckDefault"
                                        <?= $shop_for_edit['shop_is_active'] ? 'checked' : '' ?>
                                    >
                                    <label class="form-check-label" for="shop_is_active">
                                        Enable shop
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? endif; ?>

            <div class="col">
                <div class="mb-3">
                    <label for="shop_address" class="form-label">Address</label>
                    <textarea name="shop_address" class="form-control"
                              rows="4"><?= $shop_for_edit['shop_address']; ?></textarea>
                </div>
            </div>

        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>
