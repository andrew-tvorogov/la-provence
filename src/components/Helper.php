<?php

/**
 * Класс со вспомогательными методами
 */
class Helper
{

    static public function escape($str)
    {
        return htmlentities($str);
    }

}
