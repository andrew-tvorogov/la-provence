<?php

/**
 * Маршрутизатор для вызова необходимого контроллера
 */
class Router
{

    private $routes;

    public function __construct()
    {
        include_once('config/routes.php');
        $this->routes = $routes;
    }

    /**
     * Выполняет маршрутизацию в соответствии с заданными маршрутами.
     */
    public function run()
    {
        $requestedUrl = trim($_SERVER['REQUEST_URI'], '/');
        //$patternIndex = SITE_ROOT . "index.*([^0-9])+$"; // если вошли в корень сайта или корень плюс index*
        //if ( $requestedUrl === trim(SITE_ROOT, '/') OR preg_match("~$patternIndex~", $requestedUrl) )
        if ($requestedUrl === trim(SITE_ROOT, '/')) {
            $requestedUrl = SITE_ROOT . 'bouquets/index/start'; // переходим на стартовую страницу с баннером
        }

        $urlFound = false; // начальное значение - url не найден
        foreach ($this->routes as $controller => $availableRoutes) {
            foreach ($availableRoutes as $availableRoute => $actionWithParameters) {
                if (preg_match("~$availableRoute~", $requestedUrl)) {
                    $actionWithParameters = preg_replace("~$availableRoute~", $actionWithParameters, $requestedUrl);
                    $actionWithParameters = str_replace(SITE_ROOT, '', $actionWithParameters);
                    $actionWithParametersArray = explode('/', $actionWithParameters);
                    $selectedController = new $controller();
                    $action = array_shift($actionWithParametersArray);
                    $selectedAction = 'action' . ucfirst($action);
                    call_user_func_array(array($selectedController, $selectedAction), $actionWithParametersArray);
                    $urlFound = true; // url найден!
                    break 2;
                }
            }
        }
        if ($urlFound === false) {
            header('Location: ' . FULL_SITE_ROOT . 'errors/404');
        }
    }
}