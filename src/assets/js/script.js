"use strict";

console.log('script.js has been loaded');

// инициализация popover
$(function () {
    $('[data-toggle="popover"]').popover()
});

//подгонка капчи
// if (captcha_container){
//     let recap = document.querySelector('#captcha_container');
//     console.log(recap.getBoundingClientRect());
//
//     let reCaptchaWidth = 302;
//     let containerWidth = recap.style.width;
//
//     console.log(containerWidth);
//     if (reCaptchaWidth > containerWidth) {
//         let reCaptchaScale = containerWidth / reCaptchaWidth;
//         recap.style.transform.scale = reCaptchaScale;
//         recap.style.transformOrigin = 'left top';
//     }
// }


// запрос на добавление bouquet в favorites
function favoritesAddRemove(bouquetId, userId, siteRoot) {
    let answer = '';
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            answer += this.responseText;
            //console.log(answer);
            location.reload();
            //cardInFavoritesExistCheck();
        }
    };
    let fullSiteRoot = siteRoot;
    let req = fullSiteRoot + `favorite/switch/${bouquetId}/${userId}`;
    xhr.open('POST', req, true);
    xhr.send();
}

// TODO: реализовать реакцию карточки без перегрузки страницы
// реакция карточки на существование букета в favorites
function cardInFavoritesExistCheck() {
    if (answer === '1') {
        console.log('bouquet exists in favorites');
        // signupEmailLable.innerHTML = "Email exists";
        // signupEmail.classList.remove('is-valid');
        // signupEmail.classList.add('is-invalid');
        // signupEmailLable.style.color = "#cc223b";
        return false;
    } else if (answer === '0') {
        // signupEmailLable.innerHTML = "Email";
        // signupEmail.classList.remove('is-invalid');
        // signupEmail.classList.add('is-valid');
        // signupEmailLable.style.color = "#643939";
        console.log('bouquet does not exist in favorites');
        return true;
    }
}

