"use strict";

console.log('modal.js has been loaded');

// MODAL DIALOG
// validate Sign Up

let email = document.querySelector("#signupEmail");
let pass = document.querySelector("#signupPassword");
let passRepeat = document.querySelector("#signupPasswordRepeat");
let signUpBtn = document.querySelector("#sign_Up_Btn");
let signUpCheckBox = document.querySelector("#signupCheckBox");
let signupEmail = document.querySelector("#signupEmail");

// проверка email на допустимые символы
if (email) {
    email.addEventListener('input', function (e) {
        let oldVal = this.value; // this - это сам элемент email
        this.value = oldVal.replace(/[^0-9A-Za-z_@.-]/g, '');
    });
}

// активация кнопки Submit при валидном заполнении всех полей и checkbox
if (signUpCheckBox) {
    signUpCheckBox.addEventListener('click', function () {
        if (signUpCheckBox.checked) {
            if (passMatchCheck(pass, passRepeat) && emailExistCheck(email)) {
                signUpBtn.removeAttribute('disabled');
            }
        } else {
            signUpBtn.setAttribute('disabled', 'disabled');
        }
    });
}

// слушаем поле password repeat, сравниваем поля ввода пароля
if (passRepeat){
    passRepeat.addEventListener('blur', function (){
        passMatchCheck(pass, passRepeat);
    })
}

// сравниваем поля ввода пароля
function passMatchCheck(password, passwordRepeat) {
    if (password.value !== '' && password.value === passwordRepeat.value) {
        console.log('passwords match');
        signupPassword.classList.remove('is-invalid');
        signupPassword.classList.add('is-valid');
        signupPasswordRepeat.classList.remove('is-invalid');
        signupPasswordRepeat.classList.add('is-valid');
        signupPasswordLable.style.color = "#643939";
        signupPasswordRepeatLable.style.color = "#643939";
        return true;
    } else {
        console.log('passwords NOT match');
        signupPassword.classList.remove('is-valid');
        signupPassword.classList.add('is-invalid');
        signupPasswordRepeat.classList.remove('is-valid');
        signupPasswordRepeat.classList.add('is-invalid');
        signupPasswordLable.style.color = "#cc223b";
        signupPasswordRepeatLable.style.color = "#cc223b";
        return false;
    }
}

// проверка существования email, запрос к базе xhr
let answer;
if (signupEmail){
    signupEmail.addEventListener("blur", function () {
        answer = '';
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                answer += this.responseText;
                console.log(answer);
                emailExistCheck();
            }
        };
        // TODO: разобраться с путями!!! абсолютный путь не годится
        //let req = `http://localhost:3000/la_provence/build/email/${email.value}`;
        let req = `email/${email.value}`;
        xhr.open('POST', req, true);
        xhr.send();
    });
}

// реакция формы на результат проверки существования email
function emailExistCheck() {
    if (answer === '1') {
        console.log('email exists');
        signupEmailLable.innerHTML = "Email exists";
        signupEmail.classList.remove('is-valid');
        signupEmail.classList.add('is-invalid');
        signupEmailLable.style.color = "#cc223b";
        return false;
    } else if (answer === '0') {
        signupEmailLable.innerHTML = "Email";
        signupEmail.classList.remove('is-invalid');
        signupEmail.classList.add('is-valid');
        signupEmailLable.style.color = "#643939";
        console.log('email does not exists');
        return true;
    }
}