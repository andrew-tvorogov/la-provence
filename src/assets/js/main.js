// Импортируем jQuery
//= ../../../node_modules/jquery/dist/jquery.js

// Импортируем Popper
//= ../../../node_modules/popper.js/dist/umd/popper.js

// Импортируем необходимые js-файлы Bootstrap 4 часть можно отключить
//= ../../../node_modules/bootstrap/js/dist/util.js
//= ../../../node_modules/bootstrap/js/dist/alert.js
//= ../../../node_modules/bootstrap/js/dist/button.js
//= ../../../node_modules/bootstrap/js/dist/carousel.js
//= ../../../node_modules/bootstrap/js/dist/collapse.js
//= ../../../node_modules/bootstrap/js/dist/dropdown.js
//= ../../../node_modules/bootstrap/js/dist/modal.js
//= ../../../node_modules/bootstrap/js/dist/tooltip.js
//= ../../../node_modules/bootstrap/js/dist/popover.js
//= ../../../node_modules/bootstrap/js/dist/scrollspy.js
//= ../../../node_modules/bootstrap/js/dist/tab.js
//= ../../../node_modules/bootstrap/js/dist/toast.js

// Импортируем файл modal.js с кодом валидации полей окна регистрации пользователя
//= modal.js

// Импортируем файл cookie.js для работы с cookie
//= cookie.js

// Импортируем файл sort.js с кодом сортировки карточек по цвету, цене, размеру, категории
//= sort.js

// Импортируем файл cart.js с кодом обработки данных корзины
//= cart.js

// Импортируем файл ckeditor_starter.js - запускающий сторонний текстовый редактор
// и сам editor
//= ../../../node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js
//= ckeditor_starter.js

// Импорт cookie-notice
//= ../../../node_modules/cookie-notice/dist/cookie.notice.js
//= cookie-notice-conf.js

// Импортируем основной файл script.js
//= script.js