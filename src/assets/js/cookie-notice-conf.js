new cookieNoticeJS({

    // Localizations of the notice message
    'messageLocales': {
        'it': 'Custom localized message'
    },

    // Localizations of the dismiss button text
    'buttonLocales': {
        'it': 'Chiudi'
    },

    // Position for the cookie-notifier (default=bottom)
    'cookieNoticePosition': 'bottom',

    // Shows the "learn more button (default=false)
    'learnMoreLinkEnabled': false,

    // The href of the learn more link must be applied if (learnMoreLinkEnabled=true)
    'learnMoreLinkHref': '/learn/more/index.html',

    // Text for optional learn more button
    'learnMoreLinkText':{
        'en':'learn more'
    },

    // The message will be shown again in X days
    'expiresIn': 30,

    // Specify a custom font family and size in pixels
    'fontFamily': 'Montserrat',
    'fontSize': '12px',

    // Dismiss button background color
    'buttonBgColor': '#E60020',

    // Dismiss button text color
    'buttonTextColor': '#FFF',

    // Notice background color
    'noticeBgColor': '#643939',

    // Notice text color
    'noticeTextColor': '#fff',

    // the learnMoreLink color (default='#009fdd')
    'linkColor': '#f00',

    // The target of the learn more link (default='', or '_blank')
    'linkTarget': '',

    // Print debug output to the console (default=false)
    'debug': false
});