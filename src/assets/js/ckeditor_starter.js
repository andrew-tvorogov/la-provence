// если в DOM есть элемент ckeditor - подключаем его
if (document.querySelector('#ckeditor')) {
    ClassicEditor
        .create(document.querySelector('#ckeditor'), {
            toolbar: ['heading', '|', 'bold', 'italic', '|', 'bulletedList', 'numberedList', '|', 'link', '|', 'insertTable', 'undo', 'redo']
        })
        .catch(error => {
            console.error(error);
        });
}
