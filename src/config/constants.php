<?php

	define('DOMAIN', 'provence.local/');
//define('DOMAIN', 'localhost/');
	define('SCHEME', 'http://');
	define('SITE_ROOT', '');
//define('SITE_ROOT', 'la_provence/build/');
	define('FULL_SITE_ROOT', SCHEME . DOMAIN . SITE_ROOT);
	define('SITE_FILE_ROOT', 'C:/xampp/htdocs/');
	define('ASSETS', '/' . SITE_ROOT . 'assets/');
	define('CSS', ASSETS . 'css/');
    define('JS', ASSETS . 'js/');
	define('FONTS', ASSETS . 'fonts/');
	define('FAVICON', ASSETS . 'favicon/');
	define('IMG', ASSETS . 'img/');
	         
?>