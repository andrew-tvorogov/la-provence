<?php

/**
 * Массив путей к страницам
 */
$routes = array(

    'UserController' => array(
        'signup' => 'signup',
        'login' => 'login',
        'logout' => 'logout',
        'email/(.+)' => 'email/$1',
        'users/index/all' => 'index/all',
        'users/index/([0-9]+)' => 'index/$1',
        'user/delete/([0-9]+)' => 'delete/$1',
        'user/edit/([0-9]+)' => 'edit/$1'
    ),

    'BannerController' => array(
        'banner/add' => 'add',
        'banner/edit/([0-9]+)' => 'edit/$1',
        'banner/enable/([0-9]+)' => 'enable/$1',
        'banner/delete/([0-9]+)' => 'delete/$1',
        'banners' => 'list'
    ),

    'InfoController' => array(
        'info/edit/([0-9]+)' => 'edit/$1',
        'info/(.+)' => 'view/$1',
        'pages/list' => 'list'
    ),

    'FavoriteController' => array(
        'favorite/switch/([0-9]+)' => 'switch/$1'
    ),

    'CabinetController' => array(
        'cabinet' => 'view'
    ),

    'EmployeeController' => array(
        'employees/list' => 'list'
    ),

    'AdminController' => array(
        'admin' => 'view'
    ),
	
	'ElfinderController' => array(
        'elfinder/view' => 'view'
    ),

    'BouquetController' => array(
        'bouquets/index/start' => 'index/start', // состояние при входе на сайт без параметров
        'bouquets/index/all' => 'index/all',
        'bouquets/index/([0-9]+)' => 'index/$1',
        'bouquets/list/([0-9]+)' => 'list/$1', // для администратора список
        'bouquet/add' => 'add',
        'bouquet/edit/([0-9]+)' => 'edit/$1',
        'bouquet/view/([0-9]+)' => 'view/$1',
        'bouquet/delete/([0-9]+)' => 'delete/$1'
    ),

    'FloristController' => array(
        'florists/all' => 'all',
        'florist/edit/([0-9]+)' => 'edit/$1',
        'florist/([0-9]+)' => 'view/$1'
    ),

    'CartController' => array(
        'cart' => 'index'
    ),

    'FeedbackController' => array(
        'feedback/add' => 'add',
        'feedback/delete/([0-9]+)' => 'delete/$1',
        'feedbacks/index/all' => 'index/all',
        'feedbacks/index/([0-9]+)' => 'index/$1',
        'feedback' => 'view'
    ),

    'OrderController' => array(
        'orders/index/all' => 'index/all',
        'orders/index/([0-9]+)' => 'index/$1',
        'order/add' => 'add',
        'order/delete/([0-9]+)' => 'delete/$1'
    ),

    'OfferController' => array(
        'offer/add' => 'add',
        'offer/edit/([0-9]+)' => 'edit/$1',
        'offer/delete/([0-9]+)' => 'delete/$1',
        'offers' => 'index'
    ),

    'ShopController' => array(
//        'shops/index/all' => 'index/all',
//        'shops/index/([0-9]+)' => 'index/$1',
        'shop/edit/([0-9]+)' => 'edit/$1',
        'shop/add' => 'add',
        'shop/delete/([0-9]+)' => 'delete/$1',
        'shops/list' => 'list'
    ),

    'ErrorController' => array(
        'errors/([0-9]+)' => 'index/$1'
    )
);

?>