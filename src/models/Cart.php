<?php

/**
 * Корзина.
 */
class Cart
{

    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Достаёт данные по лежащим в корзине (в куках) букетам
     *
     * @param string $ids Идентификаторы букетов
     * @return array Букеты в корзине.
     */
    public function getCartInfo($ids)
    {
        $idsString = implode(',', $ids);
        $query = "
    SELECT * 
    FROM `bouquets`
    WHERE `bouquet_id` IN ($idsString);
    ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Корзина из куки
     *
     * @return array Букеты в корзине.
     */
    public function bouquetsInCart()
    {
        $bouquetsCart = [];
        if (isset($_COOKIE['cart'])) {
            $cart = json_decode($_COOKIE['cart'], true);
            $ids = array_keys($cart);
            $bouquetsCartInfo = $this->getCartInfo($ids);
            foreach ($bouquetsCartInfo as &$bouquetCart) {
                $bouquetCart['bouquet_quantity'] = $cart[$bouquetCart['bouquet_id']];
            }
            $bouquetsCart = $bouquetsCartInfo;
        }
        return $bouquetsCart;
    }

    /**
     * Проверяет есть ли в массиве $bouquets букеты с $id
     * Метод нужен для вывода количества букетов на кнопку 'add to cart'.
     *
     * @param int $id Идентификатор букета
     * @param array $bouquets Массив букетов для проверки
     * @return boolean Если есть - true, иначе - false.
     */
    public function bouquetExistInCart($id, $bouquets)
    {
        foreach ($bouquets as $bouquet) {
            if ($id === $bouquet['bouquet_id']) {
                return true;
            }
        }
        return false;
    }

    /**
     * Общая сумма заказа
     *
     * @return int Общая сумма заказа.
     */
    public function summ()
    {
        $total = 0;
        foreach ($bouquetsCart as $orderTotal) {
            $total = $total + ($orderTotal['bouquet_price'] * $orderTotal['bouquet_quantity']);
        }
        return $total;
    }

}