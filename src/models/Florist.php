<?php

/**
 * Флористы.
 */
class Florist
{
    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Возвращает имена файлов фото флористов.
     *
     * @return array Массив имён файлов фото флористов.
     */
    public function getFloristsFiles()
    {
        // список файлов
        $dir = './assets/img/florists';
        $files = scandir($dir);

        $filter = "/(^\.+)/";
        $filtered_files = []; // массив для имён отфильтрованных файлов

        for ($i = 0; $i < count($files); $i++) {
            if ( !preg_match($filter, $files[$i])) { // если имя файла не начинается на . добавляем в массив
                $filtered_files[] = $files[$i];
            }
        }
        return $filtered_files;
    }

    /**
     * Возвращает массив флористов.
     *
     * @return array Все флористы.
     */
    public function getAllFlorists() {
        $query = "
		SELECT `florist_id`, `florist_name`, `florist_surname`, `florist_phone`,
			`florist_img`, `florist_hire_date`, `florist_descr`
		FROM `florists`				
	";
        $result = mysqli_query($this->db, $query);
        $florists = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $florists;
    }

    /**
     * Возвращает флориста, включая его букеты.
     *
     * @param int $id Идентификатор флориста.
     * @return array Информация о флористе.
     */
    public function getFlorist($id) {
        $query = "
		SELECT *
		FROM `florists`
		LEFT JOIN `bouquets` ON `florist_id` = `bouquet_florist_id`
        WHERE `florist_id` = $id;
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

    /**
     * Возвращает букеты флориста.
     *
     * @param int $id Идентификатор флориста.
     * @return array Информация о букетах флориста.
     */
    public function getFloristBouquets($id) {
        $query = "
		SELECT *
		FROM `bouquets`		
        WHERE `bouquet_florist_id` = $id;
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Редактирует флориста
     *
     * @param string $florist_name Имя флориста.
     * @param string $florist_surname Фамилия флориста.
     * @param string $florist_phone Телефон флориста.
     * @param string $florist_img Изображение(фото) флориста.
     * @param string $florist_descr Описание флориста.
     * @return boolean Если успешно - true
     */
    public function editFlorist(
        $id,
        $florist_name,
        $florist_surname,
        $florist_phone,
        $florist_img,
        $florist_descr
    )
    {
        $query = "UPDATE `florists` SET
                    `florist_name` = '$florist_name',
                    `florist_surname` = '$florist_surname',
                    `florist_phone` = '$florist_phone',
                    `florist_img` = '$florist_img',
                    `florist_descr` = '$florist_descr'                    
                    WHERE `florist_id` = '$id';
                    ";
        mysqli_query($this->db, $query);
        return true;
    }

}