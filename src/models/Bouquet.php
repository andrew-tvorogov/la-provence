<?php

/**
 * Манипулирует данными букетов - модификация, удаление, добавление.
 */
class Bouquet
{
    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Возвращает имена файлов букетов.
     *
     * @return array Список файлов букетов.
     */
    public function getBouquetImages()
    {
        // список файлов изображений букетов
        $dir = './assets/img/bouquets';
        $files = scandir($dir);

        $filter = "/(^\.+)/";
        $filtered_files = []; // массив для имён отфильтрованных файлов

        for ($i = 0; $i < count($files); $i++) {
            if (!preg_match($filter, $files[$i])) { // если имя файла не начинается на . добавляем в массив
                $filtered_files[] = $files[$i];
            }
        }
        return $filtered_files;
    }

    /**
     * Возвращает массив всех цветов букетов.
     *
     * @return array Цвета букетов.
     */
    public function getBouquetColors()
    {
        $query = "
		SELECT *
		FROM `colors`
        ORDER BY `color_id`;        
	";
        $result = mysqli_query($this->db, $query);
        $colors = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $colors;
    }

    /**
     * Возвращает массив всех размеров букетов.
     *
     * @return array Размеры букетов.
     */
    public function getBouquetSizes()
    {
        $query = "
		SELECT *
		FROM `sizes`;        
	";
        $result = mysqli_query($this->db, $query);
        $sizes = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $sizes;
    }

    /**
     * Возвращает массив всех категорий букетов.
     *
     * @return array Категории букетов.
     */
    public function getBouquetCategories()
    {
        $query = "
		SELECT *
		FROM `categories`;        
	";
        $result = mysqli_query($this->db, $query);
        $categories = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $categories;
    }

    /**
     * Возвращает количество букетов, не включая удаленные.
     *
     * @return int Количество букетов.
     */
    public function getAllCount()
    { // сколько всего букетов
        $query = "
        SELECT COUNT(*) AS `count`
        FROM `bouquets`
        WHERE `bouquet_is_deleted` = 0;
        ";
        $result = mysqli_query($this->db, $query);
        $count = mysqli_fetch_assoc($result)['count'];
        return $count;
    }

    /**
     * Возвращает количество букетов, включая удаленные,
     * нужно для администратора.
     *
     * @return int Количество букетов.
     */
    public function getAllCountWithDeleted()
    { // сколько всего букетов
        $query = "
        SELECT COUNT(*) AS `count`
        FROM `bouquets`;        
        ";
        $result = mysqli_query($this->db, $query);
        $count = mysqli_fetch_assoc($result)['count'];
        return $count;
    }

    /**
     * Возвращает массив букетов, включая удаленные,
     * нужно для администратора.
     *
     * @return array Все букеты.
     */
    public function getAllWithDeleted() // все букеты, включая удалённые
    {
        $query = "
		SELECT `bouquet_id` AS `id`, `bouquet_name`, `bouquet_price`, `bouquet_favorites`,
			`bouquet_img`, `bouquet_descr`, `bouquet_is_deleted`, 
		    `florist_name`,
		    `bouquet_category_id`, `category_name`,
		    `bouquet_size_id`, `size_name`,
		    `bouquet_color_id`, `color_name`
		FROM `bouquets`		
		LEFT JOIN `florists` ON `bouquet_florist_id` = `florist_id`
		LEFT JOIN `categories` ON `bouquet_category_id` = `category_id`
		LEFT JOIN `sizes` ON `bouquet_size_id` = `size_id`
		LEFT JOIN `colors` ON `bouquet_color_id` = `color_id`		
        ORDER BY `bouquet_price`;
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Возвращает массив букетов, включая удаленные.
     * С какой страницы $page, сколько штук выводить $limit
     * нужно для администратора.
     *
     * @param int $page Текущая страница.
     * @param int $limit Сколько на странице.
     * @return array Все отобранные букеты.
     */
    public function getAllWithLimitWithDeleted($page, $limit)
    {
        $query = " 
       SELECT `bouquet_id` AS `id`, `bouquet_name`, `bouquet_price`, `bouquet_favorites`,
			`bouquet_img`, `bouquet_descr`, `bouquet_is_deleted`, 
		    `florist_name`,
		    `bouquet_category_id`, `category_name`,
		    `bouquet_size_id`, `size_name`,
		    `bouquet_color_id`, `color_name`
		FROM `bouquets`		
		LEFT JOIN `florists` ON `bouquet_florist_id` = `florist_id`
		LEFT JOIN `categories` ON `bouquet_category_id` = `category_id`
		LEFT JOIN `sizes` ON `bouquet_size_id` = `size_id`
		LEFT JOIN `colors` ON `bouquet_color_id` = `color_id`	                
        ORDER BY `bouquet_price`
        LIMIT $page, $limit;
        ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Возвращает массив букетов, не включая удаленные.
     *
     * @return array Все букеты.
     */
    public function getAll()
    {
        $query = "
		SELECT `bouquet_id` AS `id`, `bouquet_name`, `bouquet_price`, `bouquet_favorites`,
			`bouquet_img`, `bouquet_descr`, `bouquet_is_deleted`, 
		    `florist_name`,
		    `bouquet_category_id`, `category_name`,
		    `bouquet_size_id`, `size_name`,
		    `bouquet_color_id`, `color_name`
		FROM `bouquets`		
		LEFT JOIN `florists` ON `bouquet_florist_id` = `florist_id`
		LEFT JOIN `categories` ON `bouquet_category_id` = `category_id`
		LEFT JOIN `sizes` ON `bouquet_size_id` = `size_id`
		LEFT JOIN `colors` ON `bouquet_color_id` = `color_id`
		WHERE `bouquet_is_deleted` = 0
        ORDER BY `bouquet_price`;
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Возвращает массив букетов, не включая удаленные.
     * С какой страницы $page, сколько штук выводить $limit
     *
     * @param int $page Текущая страница.
     * @param int $limit Сколько на странице.
     * @return array Все отобранные букеты.
     */
    public function getAllWithLimit($page, $limit)
    {
        $query = " 
       SELECT `bouquet_id` AS `id`, `bouquet_name`, `bouquet_price`, `bouquet_favorites`,
			`bouquet_img`, `bouquet_descr`, `bouquet_is_deleted`, 
		    `florist_name`,
		    `bouquet_category_id`, `category_name`,
		    `bouquet_size_id`, `size_name`,
		    `bouquet_color_id`, `color_name`
		FROM `bouquets`		
		LEFT JOIN `florists` ON `bouquet_florist_id` = `florist_id`
		LEFT JOIN `categories` ON `bouquet_category_id` = `category_id`
		LEFT JOIN `sizes` ON `bouquet_size_id` = `size_id`
		LEFT JOIN `colors` ON `bouquet_color_id` = `color_id`
		WHERE `bouquet_is_deleted` = 0                
        ORDER BY `bouquet_price`
        LIMIT $page, $limit;
        ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Добавляет букет
     *
     * @param string $bouquet_name Имя букета.
     * @param string $bouquet_price Цена букета.
     * @param string $bouquet_image Изображение букета.
     * @param string $bouquet_description Описание букета.
     * @param int $bouquet_florist_id Идентификатор флориста.
     * @param int $bouquet_color_id Идентификатор цвета.
     * @param int $bouquet_size_id Идентификатор размера.
     * @param int $bouquet_category_id Идентификатор категории.
     */
    public function addBouquet(
        $bouquet_name,
        $bouquet_price,
        $bouquet_image,
        $bouquet_description,
        $bouquet_florist_id,
        $bouquet_color_id,
        $bouquet_size_id,
        $bouquet_category_id
    ) {
        $query = "INSERT INTO `bouquets`
                SET `bouquet_name` = '$bouquet_name',
                    `bouquet_price` = '$bouquet_price',
                    `bouquet_img` = '$bouquet_image',
                    `bouquet_descr` = '$bouquet_description',
                    `bouquet_florist_id` = '$bouquet_florist_id',
                    `bouquet_color_id` = '$bouquet_color_id',
                    `bouquet_size_id` = '$bouquet_size_id',
                    `bouquet_category_id` = '$bouquet_category_id';
        ";
        mysqli_query($this->db, $query);
        return true;
    }

    /**
     * Редактирует букет
     *
     * @param string $bouquet_new_name Имя букета.
     * @param string $bouquet_new_price Цена букета.
     * @param string $bouquet_new_image Изображение букета.
     * @param string $bouquet_new_description Описание букета.
     * @param int $bouquet_new_florist_id Идентификатор флориста.
     * @param int $bouquet_new_color_id Идентификатор цвета.
     * @param int $bouquet_new_size_id Идентификатор размера.
     * @param int $bouquet_new_category_id Идентификатор категории.
     * @param int $id Идентификатор букета.
     */
    public function editBouquet(
        $bouquet_new_name,
        $bouquet_new_price,
        $bouquet_new_image,
        $bouquet_new_description,
        $bouquet_new_florist_id,
        $bouquet_new_color_id,
        $bouquet_new_size_id,
        $bouquet_new_category_id,
        $id
    ) {
        $query = "UPDATE `bouquets` SET
                    `bouquet_name` = '$bouquet_new_name',
                    `bouquet_price` = '$bouquet_new_price',
                    `bouquet_img` = '$bouquet_new_image',
                    `bouquet_descr` = '$bouquet_new_description',
                    `bouquet_florist_id` = '$bouquet_new_florist_id',
                    `bouquet_color_id` = '$bouquet_new_color_id',
                    `bouquet_size_id` = '$bouquet_new_size_id',
                    `bouquet_category_id` = '$bouquet_new_category_id'                    
                    WHERE `bouquet_id` = '$id';
                    ";
        mysqli_query($this->db, $query);
    }

    /**
     * Удаляет букет
     *
     * @param int $id Идентификатор букета.
     */
    public function deleteBouquet($id)
    {
        $query = "UPDATE `bouquets` SET
                    `bouquet_is_deleted` = '1'                                        
                    WHERE `bouquet_id` = '$id';
                    ";
        mysqli_query($this->db, $query);
    }

    /**
     * Возвращает букет
     *
     * @param int $id Идентификатор букета.
     * @return array Данные букета.
     */
    public function getBouquetById($id)
    {
        $query = "
		SELECT *
		FROM `bouquets`
        WHERE `bouquet_id` = $id;        
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

    /**
     * Возвращает полную информацию по букету, включая флориста, категорию, цвет, размер
     *
     * @param int $id Идентификатор букета.
     * @return array Данные букета.
     */
    public function getBouquetByIDFullInfo($id) // все о букете
    {
        $query = "
		SELECT `bouquet_id` AS `id`, `bouquet_name`, `bouquet_price`, `bouquet_favorites`,
			`bouquet_img`, `bouquet_descr`, `bouquet_is_deleted`, 
		    `florist_name`,
		    `bouquet_category_id`, `category_name`,
		    `bouquet_size_id`, `size_name`,
		    `bouquet_color_id`, `color_name`
		FROM `bouquets`		
		LEFT JOIN `florists` ON `bouquet_florist_id` = `florist_id`
		LEFT JOIN `categories` ON `bouquet_category_id` = `category_id`
		LEFT JOIN `sizes` ON `bouquet_size_id` = `size_id`
		LEFT JOIN `colors` ON `bouquet_color_id` = `color_id`
		WHERE `bouquet_id` = $id;
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

}