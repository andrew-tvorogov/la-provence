<?php

/**
 * Манипулирует данными пользователей - модификация, удаление, добавление.
 */
class User
{

    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Возвращает данные пользователя, включая адрес
     *
     * @param int $id Идентификатор пользователя.
     * @return array Данные пользователя.
     */
    public function getUserInfo($id)
    {
        $query = "SELECT * 
              FROM `users`
              LEFT JOIN `addresses` ON `user_address_id` = `address_id`
              WHERE `user_id` = $id";
        $result = mysqli_query($this->db, $query);
        $userInfo = mysqli_fetch_assoc($result);
        return $userInfo;
    }

    /**
     * Проверяет, существует ли email в базе пользователей
     *
     * @param string $email Электронный адрес.
     * @return boolean True - если существует, false - если нет.
     */
    public function checkIfEmailExists($email)
    {
        $query = "
        SELECT COUNT(*) AS `count`
        FROM `users`
        WHERE `user_email` = '$email';
        ";
        $result = mysqli_query($this->db, $query);
        $count = mysqli_fetch_assoc($result)['count'];
        return ($count === '1');
    }

    /**
     * Проверяет, существует ли пользователь с парой email/password
     *
     * @param string $email Электронный адрес.
     * @param string $password Пароль
     * @return boolean True - если существует, false - если нет.
     */
    public function checkIfUserExists($email, $password)
    {
        $query = "
        SELECT COUNT(*) AS `count`
        FROM `users`
        WHERE `user_email` = '$email' AND `user_password` = '$password';
        ";
        $result = mysqli_query($this->db, $query);
        $count = mysqli_fetch_assoc($result)['count'];
        return ($count === '1');
    }

    /**
     * Проверяет, удален ли пользователь
     *
     * @param int $id Идентификатор пользователя
     * @return boolean True - если существует, false - если нет.
     */
    public function checkIfUserIsDeleted($id)
    {
        $query = "
        SELECT COUNT(*) AS `count`
        FROM `users`
        WHERE `user_id` = '$id' AND `user_is_deleted` = '1';
        ";
        $result = mysqli_query($this->db, $query);
        $count = mysqli_fetch_assoc($result)['count'];
        return ($count === '1');
    }

    /**
     * Добавляет нового пользователя
     *
     * @param string $email Электронный адрес.
     * @param string $password Пароль
     * @return boolean True - если успешно добавлен, false - если нет.
     */
    public function addNewUser($email, $password)
    {
        $query = "INSERT INTO `users`
                SET `user_email` = '$email',
                    `user_password` = '$password';
        ";
        mysqli_query($this->db, $query);
        return true;
    }

    /**
     * Возвращает пользователя
     *
     * @param string $email Электронный адрес.
     * @param string $hash_password Пароль
     * @return array Данные пользователя.
     */
    public function getUser($email, $hash_password)
    {
        $query = "
					SELECT `user_id`, `user_name`, `user_email`, `user_is_admin`
					FROM `users`
					WHERE `user_email` = '$email'
					AND `user_password` = '$hash_password';
				";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

    /**
     * Возвращает id пользователя
     *
     * @param string $email Электронный адрес.
     * @param string $password Пароль
     * @return int Идентификатор пользователя.
     */
    public function getUserId($email, $password)
    {
        return $this->getUser($email, $password)['user_id'];
    }

    /**
     * Возвращает заказы пользователя
     *
     * @param int $id Идентификатор пользователя.
     * @return array Заказы пользователя.
     */
    public function getUserOrders($id)
    {
        $query = "
		SELECT `order_id`, `delivery_name` AS `order_delivery_name`, `status_name` AS `order_status_name`, 
		       `user_name` AS `order_user_name`, `user_id`, `bouquet_name`, `bouquet_img`, `order_start_time`			
		FROM `orders`
        LEFT JOIN `deliveries` ON `delivery_id` = `order_delivery_id`
        LEFT JOIN `users` ON `order_user_id` = `user_id`
        LEFT JOIN `statuses` ON `order_status_id` = `status_id` 
		LEFT JOIN `sets` ON `set_order_id` = `order_id`
		LEFT JOIN `bouquets` ON `set_bouquet_id` = `bouquet_id`
        WHERE `order_user_id` = '$id';
	";
        $result = mysqli_query($this->db, $query);
        $orders = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $orders;
    }

    /**
     * Проверяет авторизован ли пользователь, данные для проверки их куки
     *
     * @return boolean Если авторизован - true, иначе - false.
     */
    public function checkIfAuthorized()
    {
        $isAuthorized = false;
        if (isset($_COOKIE['user']) && isset($_COOKIE['token']) && isset($_COOKIE['tokenTime'])) {
            $user_id = $_COOKIE['user'];
            $token = $_COOKIE['token'];
            $query = "
				SELECT `connect_id`, UNIX_TIMESTAMP(`connect_token_time`) AS `token_time`
				FROM `connects`
				WHERE `connect_user_id` = '$user_id'
				AND `connect_token` = '$token';
			";
            $result = mysqli_query($this->db, $query);
            $connect_info = mysqli_fetch_assoc($result);
            if ($connect_info) {
                $isAuthorized = true;
                if (time() > $connect_info['token_time']) {
                    $new_token = $this->generateToken();
                    $new_token_time = time() + 2 * 60;
                    $connect_id = $connect_info['connect_id'];
                    $query = "
						UPDATE `connects`
						SET `connect_token` = '$new_token',
							`connect_token_time` = FROM_UNIXTIME($new_token_time)
						WHERE `connect_id` = $connect_id;
					";
                    mysqli_query($this->db, $query);
                    setcookie('token', $new_token, time() + 2 * 24 * 3600, '/');
                }
            }
        }
        return $isAuthorized;
    }

    /**
     * Проверяет является ли пользователь администратором, данные из куки.
     * Проверка по базе
     *
     * @return boolean Если да - true, иначе - false.
     */
    public static function checkIfAdmin()
    {
        $isAdmin = false;
        if (isset($_COOKIE['user']) && isset($_COOKIE['token']) && isset($_COOKIE['tokenTime'])) {
            $userId = $_COOKIE['user']; // userId из cookie
            include('config/db.php');
            $connection = mysqli_connect($db['host'], $db['user'], $db['password'], $db['db_name']);
            mysqli_set_charset($connection, 'utf8');
            $query = "SELECT *
                        FROM `users`
                       WHERE `user_is_admin` = 1 
                         AND `user_id` = '$userId';
                   ";
            $result = mysqli_query($connection, $query);
            $userIsAdminFlagExist = mysqli_fetch_assoc($result);
            if ($userIsAdminFlagExist) {
                $isAdmin = true;
            }
        }
        return $isAdmin;
    }

    /**
     * Генерирует токен
     *
     * @return string Токен.
     */
    public function generateToken($size = 32)
    {
        $symbols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
        $symbolsLength = count($symbols);
        $token = '';
        for ($i = 0; $i < $size; $i++) {
            $token .= $symbols[rand(0, $symbolsLength - 1)];
        }
        return $token;
    }

    /**
     * Добавляет токен в базу для пользователя
     *
     * @param int $userId Идентификатор пользователя.
     * @param string $token Тело токена
     * @param string $tokenTime Время создания токена
     * @return boolean Если успешно добавлен - true
     */
    public function addToken($userId, $token, $tokenTime)
    {
        // TODO: проверить нужно ли хранить session_id
        session_start();
        $session_id = session_id();
        $query = "INSERT INTO `connects`
                  SET `connect_user_id` = $userId,
                      `connect_token` = '$token',
                      `connect_session_id` = '$session_id',
                      `connect_token_time` = FROM_UNIXTIME($tokenTime);
                        ";
        mysqli_query($this->db, $query);
        return true;
    }

    /**
     * Считает количество пользователей
     *
     * @return int Количество пользователей
     */
    public function getAllCount()
    {
        $query = "
        SELECT COUNT(*) AS `count`
        FROM `users`;
        ";
        $result = mysqli_query($this->db, $query);
        $count = mysqli_fetch_assoc($result)['count'];
        return $count;
    }

    /**
     * Возвразает всех пользователей.
     *
     * @return array Все пользователи.
     */
    public function getAll()
    {
        $query = "
        SELECT *
        FROM `users`;
        ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Возвращает массив пользователей.
     * С какой страницы $page, сколько штук выводить $limit
     * нужно для администратора.
     *
     * @param int $page Текущая страница.
     * @param int $limit Сколько на странице.
     * @return array Все отобранные пользователи.
     */
    public function getAllWithLimit($page, $limit)
    {
        $query = " 
       SELECT *
		FROM `users`                   
        LIMIT $page, $limit;
        ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Переключает удаленного пользователя из состояния удаленный в существующий и обратно.
     * Нужно для администратора.
     *
     * @param int $id Идентификатор пользователя.
     * @return boolean True, если успешно.
     */
    public function userDeleteSwitch($id)
    {
        if ($this->checkIfUserIsDeleted($id)) {
            $query = "UPDATE `users` 
                 SET `user_is_deleted` = '0'                     
               WHERE `user_id` = $id";
            mysqli_query($this->db, $query);
            return true;
        } else {
            $query = "UPDATE `users` 
                 SET `user_is_deleted` = '1'                     
               WHERE `user_id` = $id";
            mysqli_query($this->db, $query);
            return true;
        }
    }

    /**
     * Редактирует данные пользователя.
     *
     * @param int $id Идентификатор пользователя.
     * @param string $user_name Имя пользователя.
     * @param string $user_email Электронный адрес пользователя.
     * @param string $user_phone Телефон пользователя.
     * @param int $user_is_admin Админ ли пользователь.
     * @param int $user_is_deleted Удален ли пользователь.
     * @return boolean True, если успешно.
     */
    public function editUser(
        $user_name,
        $user_email,
        $user_phone,
        $user_is_admin,
        $user_is_deleted,
        $id
    ) {
        $query = "UPDATE `users` SET
                    `user_name` = '$user_name',
                    `user_email` = '$user_email',
                    `user_phone` = '$user_phone',
                    `user_is_admin` = '$user_is_admin',
                    `user_is_deleted` = '$user_is_deleted'                    
                    WHERE `user_id` = '$id';
                    ";
        mysqli_query($this->db, $query);
        return true;
    }

}