<?php

/**
 * Избранное.
 */
class Favorite
{
    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Информация о favorites для кабинета.
     *
     * @param int $userId Идентификатор пользователя.
     * @return array Избранное для конкретного пользователя.
     */
    public function getFavorites($userId)
    {
        $query = "
    		SELECT *
	    	FROM `favorites`
    		LEFT JOIN `bouquets` ON `bouquet_id` = `favorite_bouquet_id`
            WHERE `favorite_user_id` = $userId;        
    	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Проверка - находится ли букет в избранном пользователя.
     *
     * @param int $userId Идентификатор пользователя.
     * @param int $bouquetId Идентификатор букета.
     * @return array Если массив пустой - нет в избранном, если содержит элемент - есть в избранном.
     */
    public function checkIfAlreadyInFavorites($bouquetId, $userId)
    {
        $query = "
    		SELECT *
    		FROM `favorites`
            WHERE `favorite_bouquet_id` = $bouquetId AND `favorite_user_id` = $userId;        
    	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Удаление букета из избранного пользователя.
     *
     * @param int $userId Идентификатор пользователя.
     * @param int $bouquetId Идентификатор букета.
     */
    public function favoriteDelete($bouquet_id, $user_id){
        $query = "
		            DELETE FROM `favorites`
                    WHERE `favorite_bouquet_id` = $bouquet_id AND
                          `favorite_user_id` = $user_id;
	                ";
        mysqli_query($this->db, $query);
    }

    /**
     * Добавление букета в избранное пользователя.
     *
     * @param int $user_id Идентификатор пользователя.
     * @param int $bouquet_id Идентификатор букета.
     */
    public function favoriteAdd($bouquet_id, $user_id){
        $query = "
		            INSERT INTO `favorites`
                    SET `favorite_bouquet_id` = $bouquet_id,
                        `favorite_user_id` = $user_id;
	                ";
        mysqli_query($this->db, $query);
    }

    /**
     * Сколько всего отложено в избранное.
     * Использован в AdminController для вывода статистики
     *
     * @return int Количество в избранном.
     */
    public function getAllCount()
    {
        $query = "
        SELECT COUNT(*) AS `count`
        FROM `favorites`;
        ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result)['count'];
    }

}