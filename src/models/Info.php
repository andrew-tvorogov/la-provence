<?php

/**
 * Генерация контента info страниц, страниц со статическим содержимым.
 */
class Info
{
    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Контент конкретной страницы из базы данных по её пути.
     *
     * @param string $content_path Путь.
     * @return array Контент страницы.
     */
    public function getPageContent($content_path)
    {
        $query = "SELECT `content_id`, `content_header`, `content_path`, `section_header`, `section_id`, `content_text`, `editable` 
              FROM `static_content`
              LEFT JOIN `static_sections` ON `section_id` = `content_section_id`
              WHERE `content_path` = '$content_path';
              ";
        $result = mysqli_query($this->db, $query);
        $page = mysqli_fetch_assoc($result);
        return $page;
    }

    /**
     * Массив данных всех страниц, включая информацию о секции
     *
     * @return array Данные всех страниц.
     */
    public function getPages()
    {
        $query = "SELECT * 
              FROM `static_content`
              LEFT JOIN `static_sections` ON `section_id` = `content_section_id`
              ORDER BY `content_module`
              ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Контент конкретной страницы по её id.
     *
     * @param string $id Идентификатор страницы.
     * @return array Контент страницы.
     */
    public function getPageById($id)
    {
        $query = "SELECT * 
              FROM `static_content`
              LEFT JOIN `static_sections` ON `section_id` = `content_section_id`
              WHERE `content_id` = $id;
              ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

    /**
     * Тип модуля - header/footer.
     *
     * @return array Виды модулей (1 - header, 2 - footer).
     * deprecated
     */
    public function getContentModules()
    {
        $query = "SELECT DISTINCT (`content_module`) 
              FROM `static_content`                          
              ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Список секций, необходим для заголовков меню footer.
     *
     * @return array Секции для футера.
     * deprecated
     */
    public function getContentSections()
    {
        $query = "SELECT * 
              FROM `static_sections`                          
              ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Редактирование контента конкретной страницы.
     *
     * @param int $id Идентификатор страницы.
     * @param string $new_header Обновленный header
     * @param string $new_text Обновленный текст
     */
    public function editPage(
        $id,
        $new_header,
        $new_text
    ) {
        $query = "UPDATE `static_content` SET
                    `content_header` = '$new_header',
                    `content_text` = '$new_text'                                                         
                    WHERE `content_id` = '$id';
                    ";
        mysqli_query($this->db, $query);
    }

}