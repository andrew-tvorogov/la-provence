<?php

/**
 * Специальные предложения.
 */
class Offer
{
    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Возвращает имена файлов специальных предложений.
     *
     * @return array Список файлов специальных предложений.
     */
    public function getOfferFiles()
    {
        // список файлов оффера
        $dir = './assets/img/offers';
        $files = scandir($dir);

        $filter = "/(^\.+)/";
        $filtered_files = []; // массив для имён отфильтрованных файлов

        for ($i = 0; $i < count($files); $i++) {
            if (!preg_match($filter, $files[$i])) { // если имя файла не начинается на . добавляем в массив
                $filtered_files[] = $files[$i];
            }
        }
        return $filtered_files;
    }

    /**
     * Возвращает все активные специальные предложения.
     *
     * @return array Массив специальных предложений.
     */
    public function getActiveOffers() // все активные предложения
    {
        $query = "
		SELECT *
		FROM `offers`				
		WHERE `offer_is_active` = 1;        
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Возвращает все специальные предложения.
     *
     * @return array Массив специальных предложений.
     */
    public function getAllOffers() // все предложения
    {
        $query = "
		SELECT *
		FROM `offers`;        
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Возвращает специальное предложение по id.
     *
     * @return int Идентификатор специального предложения.
     */
    public function getOfferById($id)
    {
        $query = "
		SELECT *
		FROM `offers`				
		WHERE `offer_id` = $id;        
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

    /**
     * Добавляет специальное предложение.
     *
     * @param string $offer_img Изображение специального предложения.
     * @param boolean $offer_is_active Статус специального предложения. 1 - активно, 0 - не активно
     */
    public function addOffer(
        $offer_img,
        $offer_is_active
    ) {

        $query = "INSERT INTO `offers`
                SET `offer_img` = '$offer_img',                    
                    `offer_is_active` = '$offer_is_active';
        ";
        mysqli_query($this->db, $query);
    }

    /**
     * Редактирует специальное предложение.
     *
     * @param int $id Идентификатор специального предложения.
     * @param string $offer_img Изображение специального предложения.
     * @param boolean $offer_is_active Статус специального предложения. 1 - активно, 0 - не активно.
     */
    public function editOffer(
        $id,
        $offer_img,
        $offer_is_active
    ) {
        $query = "UPDATE `offers` SET
                    `offer_img` = '$offer_img',                
                    `offer_is_active` = '$offer_is_active'                                        
                    WHERE `offer_id` = '$id';
                    ";
        mysqli_query($this->db, $query);
    }

    /**
     * Удаляет специальное предложение.
     *
     * @param int $id Идентификатор специального предложения.
     */
    public function deleteOffer($id)
    {
        $query = "
            DELETE FROM `offers`
            WHERE `offer_id` =  $id;
            ";
        mysqli_query($this->db, $query);
    }

}