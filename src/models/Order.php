<?php

/**
 * Заказы.
 */
class Order
{
    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Информация обо всех заказах, включая информацию о доставке, пользователях, статусах
     *
     * @return array Все заказы.
     */
    public function getOrders()
    {
        $query = "
		SELECT `order_id`, `delivery_name` AS `order_delivery_name`, `status_name` AS `order_status_name`, 
		       `user_name` AS `order_reguser_name`, `user_id`, 
		       `order_user_name`, `order_user_phone`, `order_delivery_address`, `order_delivery_comment`,
		       `order_start_time`, `order_finish_time`, `user_phone` as `order_reguser_phone`
		FROM `orders`
        LEFT JOIN `deliveries` ON `delivery_id` = `order_delivery_id`
        LEFT JOIN `users` ON `order_user_id` = `user_id`
        LEFT JOIN `statuses` ON `order_status_id` = `status_id` 
        WHERE `user_is_deleted` = 0
        ORDER BY `order_id`;
	";
        $result = mysqli_query($this->db, $query);
        $orders = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $orders;
    }

    /**
     * Информация обо всех наборах к заказу
     *
     * @param int $order Идентификатор заказа
     * @return array Все наборы.
     */
    public function getOrderSets($order)
    {
        $query = "
		SELECT *
		FROM `sets`                         
        WHERE `set_order_id` = $order;
	    ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Создание заказа
     *
     * @param int $delivery_id Идентификатор доставки.
     * @param int $user_id Идентификатор пользователя.
     * @param string $delivery_address Адрес доставки.
     * @param string $comment Комментарий к заказу.
     * @param string $user_name Имя заказчика.
     * @param string $user_phone Телефон заказчика.
     * @param string $delivery_date Дата доставки.
     * @param string $card_message Сообщение на карточке.
     * @param array $bouquetsCart id букетов из корзины.
     * @return int Идентификатор заказа.
     */
    public function createOrder(
        $delivery_id,
        $user_id,
        $delivery_address,
        $comment,
        $user_name,
        $user_phone,
        $delivery_date,
        $card_message,
        $bouquetsCart
    ) {
        // создать order
        $query = "
        INSERT INTO `orders` (`order_delivery_id`, `order_user_id`, `order_delivery_address`, `order_delivery_comment`, `order_user_name`, `order_user_phone`, `order_delivery_date`,
                                `order_card_message`) 
        VALUES ($delivery_id, $user_id, '$delivery_address', '$comment', '$user_name', '$user_phone', '$delivery_date',
                                '$card_message');
        ";

        mysqli_query($this->db, $query);
        $order_id = mysqli_insert_id($this->db); // id нового order'a

        // создание set'ов к order
        foreach ($bouquetsCart as $set) {
            $bouquet_id = $set['bouquet_id'];
            $bouquet_quantity = $set['bouquet_quantity'];
            $query = "
            INSERT INTO `sets` (`set_bouquet_id`, `set_order_id`, `set_count`) 
            VALUES ( $bouquet_id, $order_id, $bouquet_quantity);
            ";
            //echo $query;
            mysqli_query($this->db, $query);
        }
        return $order_id;
    }

    /**
     * Информация заказе
     *
     * @param int $id Идентификатор заказа.
     * @return array Информация о заказе.
     */
    public function viewOrder($id)
    {
        // TODO: добавить просмотр деталей заказа
    }

    /**
     * Редактирование заказа
     *
     * @param int $id Идентификатор заказа.
     */
    public function editOrder($id)
    {
        // TODO: добавить редактирование заказа
    }

    /**
     * Удаление заказа
     *
     * @param int $order_id Идентификатор заказа.
     */
    public function deleteOrder($order_id)
    {
        // удаление set'ов к order
        $query = "
            DELETE FROM `sets`
            WHERE `set_order_id` =  $order_id;
            ";
        mysqli_query($this->db, $query);

        // удаление order
        $query = "
            DELETE FROM `orders`
            WHERE `order_id` =  $order_id;
            ";
        mysqli_query($this->db, $query);
        return true;
    }

    /**
     * Количество заказов
     *
     * @return int Количество заказов.
     */
    public function getAllCount()
    { // сколько всего заказов
        $query = "
        SELECT COUNT(*) AS `count`
        FROM `orders`;
        ";
        $result = mysqli_query($this->db, $query);
        $count = mysqli_fetch_assoc($result)['count'];
        return $count;
    }

    /**
     * Возвращает массив заказов.
     * С какой страницы $page, сколько штук выводить $limit
     * нужно для администратора.
     *
     * @param int $page Текущая страница.
     * @param int $limit Сколько на странице.
     * @return array Все отобранные заказы.
     */
    public function getAllWithLimit($page, $limit)
    {
        $query = " 
       SELECT `order_id`, `delivery_name` AS `order_delivery_name`, `status_name` AS `order_status_name`, 
		       `user_name` AS `order_reguser_name`, `user_id`, 
		       `order_user_name`, `order_user_phone`, `order_delivery_address`, `order_delivery_comment`,
		       `order_start_time`, `order_finish_time`, `user_phone` as `order_reguser_phone`
		FROM `orders`
        LEFT JOIN `deliveries` ON `delivery_id` = `order_delivery_id`
        LEFT JOIN `users` ON `order_user_id` = `user_id`
        LEFT JOIN `statuses` ON `order_status_id` = `status_id`         
        ORDER BY `order_start_time`
        LIMIT $page, $limit;
        ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

}