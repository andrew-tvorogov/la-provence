<?php

/**
 * Формирует меню footer и header.
 */
class Menu
{

    /**
     * Массив данных для формирования меню footer'a
     *
     * @return array Массив меню футера.
     */
    public static function getFooterMenu()
    {
        $query = "SELECT `content_id`, `content_header`, `content_path`, `section_header`, `section_id` 
              FROM `static_content`
              LEFT JOIN `static_sections` ON `section_id` = `content_section_id`
              WHERE `content_module` = 2 AND `section_header` != ''
              ";
        $db = DB::getInstance();
        $result = mysqli_query($db, $query);
        $menu = mysqli_fetch_all($result, MYSQLI_ASSOC);

        $sections = []; // какие секции существуют
        foreach ($menu as $item) {
            if (!in_array($item['section_id'], $sections)) {
                $sections[] = $item['section_id'];
            }
        }

        $combined = []; // сборка меню
        foreach ($sections as $section) { // заполнение секций массивами с пунктами меню
            foreach ($menu as $item) {
                if ($item['section_id'] == $section) {
                    $combined[$section][] = $item;
                }
            }
        }
        return $combined;
    }

    /**
     * Массив данных для формирования меню header'а
     *
     * @return array Массив меню header.
     */
    public static function getHeaderMenu()
    {
        $query = "SELECT `content_id`, `content_header`, `content_path`, `section_header`, `section_id` 
              FROM `static_content`
              LEFT JOIN `static_sections` ON `section_id` = `content_section_id`
              WHERE `content_module` = 1
              ";
        $db = DB::getInstance();
        $result = mysqli_query($db, $query);
        $menu = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $menu;
    }

    /**
     * Перечень флористов для меню
     *
     * @return array Массив флористов для меню.
     */
    public static function getAllFloristsMenu()
    {
        $query = "
		SELECT `florist_id`, `florist_name`			
		FROM `florists`				
	";
        $db = DB::getInstance();
        $result = mysqli_query($db, $query);
        $florists = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $florists;
    }

}