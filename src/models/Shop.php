<?php

/**
 * Данные магазина - название, телефон, адрес.
 */
class Shop
{
    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Возвращает данные магазина
     *
     * @param int $id Идентификатор магазина.
     * @return array Данные магазина.
     */
    public function getShopById($id)
    {
        $query = "SELECT * 
              FROM `shops`              
              WHERE `shop_id` = $id";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

    /**
     * Возвращает данные всех магазинов
     *
     * @return array Данные всех магазинов.
     */
    public function getAllShops()
    {
        $query = "SELECT * 
              FROM `shops`;";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Возвращает активный магазин
     *
     * @return array Данные активного магазина.
     */
    public static function getActiveShop()
    {
        $query = "SELECT * 
              FROM `shops`              
              WHERE `shop_is_active` = '1'";
        $db = DB::getInstance();
        $result = mysqli_query($db, $query);
        return mysqli_fetch_assoc($result);
    }

    /**
     * Редактирует магазин
     *
     * @param string $shop_name Название магазина.
     * @param string $shop_phone Телефон магазина.
     * @param string $shop_email Электронный адрес магазина.
     * @param string $shop_address Реальный адрес магазина.
     * @param string $shop_is_active Активен ли магазин.
     * @param int $id Идентификатор магазина.
     */
    public function editShop(
        $id,
        $shop_name,
        $shop_phone,
        $shop_email,
        $shop_address,
        $shop_is_active
        )
    {
        if ($shop_is_active){
            $query = "UPDATE `shops` SET                    
                    `shop_is_active` = '0';                                        
                    ";
            mysqli_query($this->db, $query);
        }
        $query = "UPDATE `shops` SET
                    `shop_id` = '$id',
                    `shop_name` = '$shop_name',
                    `shop_phone` = '$shop_phone',
                    `shop_email` = '$shop_email',
                    `shop_address` = '$shop_address',
                    `shop_is_active` = '$shop_is_active'                    
                    WHERE `shop_id` = '$id';
                    ";
        mysqli_query($this->db, $query);
        return true;
    }

    /**
     * Удаляет магазин
     *
     * @param int $id Идентификатор магазина.
     */
    public function deleteShop($id)
    {
        $query = "
            DELETE FROM `shops`
            WHERE `shop_id` =  $id;
            ";
        $result = mysqli_query($this->db, $query);
        mysqli_query($this->db, $query);
    }

}