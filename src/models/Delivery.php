<?php

/**
 * Типы доставки.
 */
class Delivery
{

    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Возвращает все типы доставки
     *
     * @return array Типы доставки.
     */
    public function getDeliveriesList()
    {
        $query = "
		SELECT *
		FROM `deliveries`;
	";
        $result = mysqli_query($this->db, $query);
        $deliveries = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $deliveries;
    }

}