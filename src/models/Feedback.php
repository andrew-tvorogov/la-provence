<?php

/**
 * Обратная связь.
 */
class Feedback
{
    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Добавление сообщения обратной связи в базу данных.
     *
     * @param string $feedbackName Имя пользователя.
     * @param string $feedbackEmail Электронная почта пользователя.
     * @param string $feedbackPhone Телефон пользователя.
     * @param string $feedbackText Сообщение.
     * @return boolean Если true - успешно выполнено добавление, false - неудача.
     */
    public function addFeedback(
        $feedbackName,
        $feedbackEmail,
        $feedbackPhone,
        $feedbackText
    ) {
        $query = "INSERT INTO `feedbacks`
                SET `feedback_name` = '$feedbackName',
                    `feedback_email` = '$feedbackEmail',
                    `feedback_phone` = '$feedbackPhone',
                    `feedback_text` = '$feedbackText';
        ";
        mysqli_query($this->db, $query);
        return true;
    }

    /**
     * Получить все записи из таблицы обратной связи.
     *
     * @return array Все сообщения обратной связи.
     */
    public function getAll()
    {
        $query = "SELECT * 
              FROM `feedbacks`;";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Возвращает массив сообщений обратной связи.
     * С какой страницы $page, сколько штук выводить $limit
     * нужно для администратора.
     *
     * @param int $page Текущая страница.
     * @param int $limit Сколько на странице.
     * @return array Сообщения в пределах page - limit.
     */
    public function getAllWithLimit($page, $limit)
    {
        $query = " 
       SELECT *
		FROM `feedbacks`                   
        LIMIT $page, $limit;
        ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Cколько всего пользователей.
     *
     * @return int Количество пользователей.
     */
    public function getAllCount()
    {
        $query = "
        SELECT COUNT(*) AS `count`
        FROM `feedbacks`;
        ";
        $result = mysqli_query($this->db, $query);
        $count = mysqli_fetch_assoc($result)['count'];
        return $count;
    }

    /**
     * Удаление сообщения обратной связи.
     *
     * @param int $id Идентификатор сообщения.
     */
    public function deleteFeedback($id)
    {
        $query = "
            DELETE FROM `feedbacks`
            WHERE `feedback_id` =  $id;
            ";
        mysqli_query($this->db, $query);
    }

}