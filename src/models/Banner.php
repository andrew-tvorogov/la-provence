<?php

/**
 * Манипулирует данными баннеров - модификация, удаление, добавление.
 */
class Banner
{
    /**
     * Соединение с БД
     **/
    private $db;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Возвращает все баннеры
     *
     * @return array Все баннеры.
     */
    public function getAllBanners()
    {
        $query = "
		SELECT *
		FROM `banners`				
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Возвращает активный баннер
     *
     * @return array Данные активного баннера.
     */
    public static function getActiveBanner()
    {
        $query = "
		SELECT *
		FROM `banners`
        WHERE `banner_is_active` = 1; 
	";
        $db = DB::getInstance();
        $result = mysqli_query($db, $query);
        return mysqli_fetch_assoc($result);
    }

    /**
     * Включает баннер
     *
     * @param int $id Идентификатор баннера.
     */
    public function enableBanner($id)
    {
        settype($id, 'integer'); // приведение к int для безопасности
        $query = "UPDATE `banners` SET                    
                    `banner_is_active` = '0';
                    ";
        mysqli_query($this->db, $query);

        $query = "UPDATE `banners` SET                    
                    `banner_is_active` = '1'
                    WHERE `banner_id` = $id;
                    ";
        mysqli_query($this->db, $query);
    }

    /**
     * Возвращает баннер
     *
     * @param int $id Идентификатор баннера.
     * @return array Данные баннера.
     */
    public function getBannerById($id)
    {
        settype($id, 'integer'); // приведение к int для безопасности
        $query = "
		SELECT *
		FROM `banners`
        WHERE `banner_id` = $id;
	";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

    /**
     * Добавляет баннер
     *
     * @param string $banner_img Изображение баннера.
     * @param string $banner_video Видео баннера.
     * @param string $banner_video_poster Видео-постер баннера.
     * @param string $banner_main_text Основной текст баннера.
     * @param string $banner_small_text Малый текст баннера.
     * @param string $banner_small_text_logo Лого баннера.
     * @param string $banner_button_text Текст кнопки баннера.
     */
    public function addBanner(
        $banner_img,
        $banner_video,
        $banner_video_poster,
        $banner_main_text,
        $banner_small_text,
        $banner_small_text_logo,
        $banner_button_text
    ) {
         // экранирование символов для исключения SQL инъекции
        $banner_img = mysqli_real_escape_string($this->db, $banner_img);
        $banner_video = mysqli_real_escape_string($this->db, $banner_video);
        $banner_video_poster = mysqli_real_escape_string($this->db, $banner_video_poster);
        $banner_main_text = mysqli_real_escape_string($this->db, $banner_main_text);
        $banner_small_text = mysqli_real_escape_string($this->db, $banner_small_text);
        $banner_small_text_logo = mysqli_real_escape_string($this->db, $banner_small_text_logo);
        $banner_button_text = mysqli_real_escape_string($this->db, $banner_button_text);
        $query = "INSERT INTO `banners` SET
                    `banner_img` = '$banner_img',
                    `banner_video` = '$banner_video',
                    `banner_video_poster` = '$banner_video_poster',
                    `banner_main_text` = '$banner_main_text',
                    `banner_small_text` = '$banner_small_text',
                    `banner_small_text_logo` = '$banner_small_text_logo',
                    `banner_button_text` = '$banner_button_text';
                    ";
        mysqli_query($this->db, $query);
        return true;
    }

    /**
     * Редактирует баннер
     *
     * @param int $banner_id Идентификатор баннера.
     * @param string $banner_img Изображение баннера.
     * @param string $banner_video Видео баннера.
     * @param string $banner_video_poster Видео-постер баннера.
     * @param string $banner_main_text Основной текст баннера.
     * @param string $banner_small_text Малый текст баннера.
     * @param string $banner_small_text_logo Лого баннера.
     * @param string $banner_button_text Текст кнопки баннера.
     */
    public function editBanner(
        $banner_id,
        $banner_img,
        $banner_video,
        $banner_video_poster,
        $banner_main_text,
        $banner_small_text,
        $banner_small_text_logo,
        $banner_button_text
    ) {
        settype($id, 'integer'); // приведение к int для безопасности
        // экранирование символов для исключения SQL инъекции
        $banner_img = mysqli_real_escape_string($this->db, $banner_img);
        $banner_video = mysqli_real_escape_string($this->db, $banner_video);
        $banner_video_poster = mysqli_real_escape_string($this->db, $banner_video_poster);
        $banner_main_text = mysqli_real_escape_string($this->db, $banner_main_text);
        $banner_small_text = mysqli_real_escape_string($this->db, $banner_small_text);
        $banner_small_text_logo = mysqli_real_escape_string($this->db, $banner_small_text_logo);
        $banner_button_text = mysqli_real_escape_string($this->db, $banner_button_text);
        $query = "UPDATE `banners` SET
                    `banner_img` = '$banner_img',
                    `banner_video` = '$banner_video',
                    `banner_video_poster` = '$banner_video_poster',
                    `banner_main_text` = '$banner_main_text',
                    `banner_small_text` = '$banner_small_text',
                    `banner_small_text_logo` = '$banner_small_text_logo',
                    `banner_button_text` = '$banner_button_text'                                        
                    WHERE `banner_id` = '$banner_id';
                    ";
        mysqli_query($this->db, $query);
        return true;
    }

    /**
     * Возвращает имена файлов баннера
     *
     * @param string $filetype Тип файл, может быть пустым - 'image' и 'video'.
     * @return array Список файлов баннера.
     */
    public function getBannerFiles($filetype = 'image') // $filetype -   video или image, по-умолчанию image
    {
        // список файлов баннера
        $dir = './assets/img/banners';
        $files = scandir($dir);

        $filter = "/(^\.+)|(.*mp4$)/";
        if ($filetype == 'video') {
            $filter = "/(^\.+)|(.*jpg$)|(.*png$)|(.*gif$)/";
        }
        $filtered_files = []; // массив для имён отфильтрованных файлов

        for ($i = 0; $i < count($files); $i++) {
            if (!preg_match($filter, $files[$i])) { // если имя файла начинается на .   убираем из массива
                $filtered_files[] = $files[$i];
            }
        }
        return $filtered_files;
    }

    /**
     * Удаляет баннер
     *
     * @param int $id Идентификатор баннера.
     */
    public function deleteBanner($id)
    {
        settype($id, 'integer'); // приведение к int для безопасности
        $query = "
            DELETE FROM `banners`
            WHERE `banner_id` =  $id;
            ";
        mysqli_query($this->db, $query);
    }

}