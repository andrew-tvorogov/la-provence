<?php

/**
 * Пагинация.
 *
 */
class Pagination {

    /**
     * Сколько всего страниц с карточками (строками)
     *
     * @param int $count Общее количество карточек, задаётся запросом к базе.
     * @param int $items_on_page Количество карточек или строк при выводе пагинации, задается в вызывающем контроллере.
     * @return int Всего страниц.
     */
    // сколько всего страниц с карточками (строками)
    public function getTotalPages($count, $items_on_page){
        $total_pages = ceil($count / $items_on_page);
        return $total_pages;
    }

    /**
     * Получает текущую страницу, если она больше диапазона - возвращает ошибку или последнюю страницу (закомментировано)
     *
     * @param int $current_page Номер страницы пагинации приходит в запросе, например: host/bouquets/index/1.
     * @param int $count Общее количество карточек, задаётся запросом к базе.
     * @param int $items_on_page Количество карточек или строк при выводе пагинации, задается в вызывающем контроллере.
     * @return int Текущая страница.
     */
    public function getCurrentPage($current_page, $count, $items_on_page) {
        $total_pages = $this->getTotalPages($count, $items_on_page);
        if ($current_page > $total_pages) { // если запрошенная страница больше, чем число страниц
            //$current_page = $total_pages;  // выводим последнюю
            header('Location: ' . FULL_SITE_ROOT . 'errors/404'); // или ошибка 404
        }
        return $current_page;
    }

    /**
     * Вывод пагинации на страницу.
     *
     * @param int $current_page Номер страницы пагинации приходит в запросе, например: host/bouquets/index/1.
     * @param int $items_on_page Количество карточек или строк при выводе пагинации, задается в вызывающем контроллере.
     * @param int $count Общее количество карточек, задаётся запросом к базе.
     * @param int $path_to_page Путь, по которому осуществляется переход, например: host/bouquets/index/1 - путь: bouquets.
     */
	public function getPagination($current_page, $items_on_page, $count, $path_to_page){
        $total_pages = $this->getTotalPages($count, $items_on_page);
		$pages = array(); // массив по количеству страниц для пагинации
		for ($i = 1; $i < $total_pages + 1; $i++) { // $i - первая страница - 1
			$pages[] = $i;
		}
        include_once('views/pagination/view.php'); // добавлена пагинация
	}	
}