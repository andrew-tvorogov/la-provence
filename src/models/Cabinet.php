<?php

/**
 * Модифицирует данные пользователя.
 */
class Cabinet
{
    private $db;

    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    /**
     * Обновляет данные пользователя в личном кабинете.
     *
     * @param string Имя пользователя.
     * @param string Телефон пользователя.
     * @param int Идентификатор пользователя.
     */
    public function userUpdate($uName, $uPhone, $uId)
    {
        $query = "UPDATE `users` 
                 SET `user_name` = '$uName',
                     `user_phone` = '$uPhone'
               WHERE `user_id` = $uId";
        mysqli_query($this->db, $query);
    }

}