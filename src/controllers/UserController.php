<?php

/**
 * Controller для передачи информации о пользователях в соответствующие View, а так же
 * для манипуляции данными - модификация, удаление, добавление.
 */
class UserController
{

    private $userModel;
    private $paginationModel;

    public function __construct()
    {
        $this->userModel = new User();
        $this->paginationModel = new Pagination();
    }

    /**
     * Формирует страницу с таблицей пользователей.
     *
     * @param int $current_page Текущая страница.
     */
    public function actionIndex($current_page)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin) {
            // блок формирования страницы
            if ($current_page == 'all' || $current_page == 0) {
                $users = $this->userModel->getAll();
                include_once('views/common/header.php');
                include_once('views/user/list.php');
                include_once('views/common/footer.php');
            } else {
                // pagination
                $cards_on_page = 6; // сколько карточек(строк) выводить на страницу
                $count = $this->userModel->getAllCount(); // сколько всего карточек
                $path_to_page = 'users/index/'; // путь к странице - 'bouquets', 'orders', 'users' и тп
                $current_page = $this->paginationModel->getCurrentPage($current_page, $count,
                    $cards_on_page); // pagination
                $users = $this->userModel->getAllWithLimit(($current_page - 1) * $cards_on_page, $cards_on_page);
                include_once('views/common/header.php');
                include_once('views/user/list.php');
                $this->paginationModel->getPagination($current_page, $cards_on_page, $count,
                    $path_to_page); // pagination
                include_once('views/common/footer.php');
            }
        } else {
            header("Location: " . FULL_SITE_ROOT . 'error/404');
        }
    }

    /**
     * Осуществляет регистрацию пользователя.
     */
    public function actionSignup()
    {
        $errors = array(); // массив для сбора ошибок при регистрации

        if (isset($_POST['email'])) {

            // функция отправки запроса для валидации CAPTCHA на сервере Google
            function SiteVerify($url)
            {
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_TIMEOUT, 15);
                curl_setopt($curl, CURLOPT_USERAGENT,
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
                $curlData = curl_exec($curl);
                curl_close($curl);
                return $curlData;
            }

            // проверка капчи
            if ($_SERVER["REQUEST_METHOD"] == "POST") {

                // проверка, пройдена ли капча (есть ли данные в POST)
                $recaptcha = $_POST['g-recaptcha-response'];
                if (!empty($recaptcha)) { // если есть данные, проверяем
                    $google_url = "https://www.google.com/recaptcha/api/siteverify";
                    $secret = '6LdSz10aAAAAAKQwu1SNvGSXB4aaeEssyV5RUrfQ';
                    $ip = $_SERVER['REMOTE_ADDR'];
                    $url = $google_url . "?secret=" . $secret . "&response=" . $recaptcha . "&remoteip=" . $ip;
                    $res = SiteVerify($url); // вызов функции curl
                    $res = json_decode($res, true); // пришёл ответ в json

                    if ($res['success']) // если успех - регистрируем пользователя
                    {

                        $userEmail = htmlentities($_POST['email']);
                        $userPassword = htmlentities($_POST['password']);
                        $userRepeatPassword = htmlentities($_POST['password_repeat']);
                        // TODO: проверка полей регуляркой: пароль, email
                        if ($userPassword !== $userRepeatPassword) {
                            $errors[] = 'Пароли не совпадают!';
                        }
                        if ($this->userModel->checkIfEmailExists($userEmail)) {
                            $errors[] = 'Такой email уже существует!';
                        }
                        $userPassword = md5($userPassword);

                        if (empty($errors)) {
                            $this->userModel->addNewUser($userEmail, $userPassword);
                            $this->actionLogin();
                        }
                        header("Location: " . FULL_SITE_ROOT);
                    } else {
                        // Проверка не пройдена, обнаружен робот!
                        header('Location: ' . FULL_SITE_ROOT . 'errors/7');
                    }
                } else {
                    // Проверка не проходилась, капча не активирована
                    header('Location: ' . FULL_SITE_ROOT . 'errors/6');
                }
            }
        } else {
            //echo 'Form is not filled correctly!';
            // данные формы не пришли, переход на главную страницу
            header("Location: " . FULL_SITE_ROOT);
        }
    }

    /**
     * Осуществляет вход пользователя в магазин.
     */
    public function actionLogin()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        $errors = array();
        if (isset($_POST['email'])) {
            //$email = Helper::escape($_POST['email']);
            //$password = Helper::escape($_POST['password']);
            $email = $_POST['email'];
            $password = $_POST['password'];
            $hash_password = md5($password);

            // проверка полей
            if ($password == '' || $email == '') {
                $errors[] = 'Не все поля заполнены!';
            }
            if (!$this->userModel->checkIfUserExists($email, $hash_password)) {
                $errors[] = 'Такой связки email/pass не существует';
            }
            if (empty($errors)) {
                $userId = $this->userModel->getUserId($email, $hash_password);
                $user = $this->userModel->getUser($email, $hash_password);
                $user_name = $user['user_name'];
                $admin = $user['user_is_admin'];

                setcookie('user', $userId, (time() + 2 * 24 * 60 * 60), '/');
                setcookie('user_name', $user_name, time() + 2 * 24 * 3600, '/');
                setcookie('admin', $admin, time() + 2 * 24 * 3600, '/');

                $token = $this->userModel->generateToken();
                $tokenTime = time() + 15 * 60;
                setcookie('token', $token, (time() + 2 * 24 * 60 * 60), '/');
                setcookie('tokenTime', $tokenTime, (time() + 2 * 24 * 60 * 60), '/');

                $this->userModel->addToken($userId, $token, $tokenTime);

                if ($admin) {
                    header('Location: ' . FULL_SITE_ROOT . 'admin');
                } else {
                    header('Location: ' . FULL_SITE_ROOT);
                }
            } else {
                header('Location: ' . FULL_SITE_ROOT . 'errors/5');
            }
        }
        return true;
    }

    /**
     * Осуществляет выход пользователя из магазина.
     */
    public function actionLogout()
    {
        if (isset($_COOKIE['user'])) {
            setcookie('user', '', (time() - 3600), '/');
            setcookie('token', '', (time() - 3600), '/');
            setcookie('tokenTime', '', (time() - 3600), '/');
            setcookie('cart', '', (time() - 3600), '/');

            // TODO: куки ниже могут отсутствовать в коде, поэтому могут быть не нужны! проверить
            setcookie('key', '', (time() - 3600), '/');
            setcookie('user_name', '', (time() - 3600), '/');
            setcookie('admin', '', (time() - 3600), '/');

            header('Location: ' . FULL_SITE_ROOT);
        }
    }

    /**
     * Проверяет электронный адрес на существование в базе данных
     *
     * @param string $email Адрес электронной почты.
     */
    public function actionEmail($email)
    {
        // получили email
        // вернули 0 если нет такого email или 1, если уже есть
        if ($this->userModel->checkIfEmailExists($email)) {
            echo '1';
            return true;
        } else {
            echo '0';
            return false;
        }
    }

    /**
     * Удаляет пользователя
     *
     * @param int $id Идентификатор пользователя.
     */
    public function actionDelete($id)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        if ($isAdmin) {
            $this->userModel->userDeleteSwitch($id);
            header("Location: " . FULL_SITE_ROOT . "users/index/all");
        } else {
            header("Location: " . FULL_SITE_ROOT . "errors/404");
        }
    }

    /**
     * Редактирует пользователя
     *
     * @param int $id Идентификатор пользователя.
     */
    public function actionEdit($id)
    {

        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin) {
            $userForEdit = $this->userModel->getUserInfo($id);

            if (isset($_POST['user_email'])) {
                $user_name = $_POST['user_name'];
                $user_email = $_POST['user_email'];
                $user_phone = $_POST['user_phone'];
                $user_is_admin = $_POST['user_is_admin'];
                $user_is_deleted = $_POST['user_is_deleted'];

                $this->userModel->editUser(
                    $user_name,
                    $user_email,
                    $user_phone,
                    $user_is_admin,
                    $user_is_deleted,
                    $id);
                header("Location: " . FULL_SITE_ROOT . "users/index/1");
            } else {
                include_once('views/common/header.php');
                include_once('views/user/edit.php');
                include_once('views/common/footer.php');
            }
        } else {
            header("Location: " . FULL_SITE_ROOT . "errors/404");
        }
    }

}