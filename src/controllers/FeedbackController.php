<?php

/**
 * Controller для управления обратной связью,
 * для манипуляции данными - модификация, удаление, добавление.
 */
class FeedbackController
{

    private $userModel;
    private $feedbackModel;
    private $paginationModel;

    public function __construct()
    {
        $this->userModel = new User();
        $this->feedbackModel = new Feedback();
        $this->paginationModel = new Pagination();
    }

    /**
     * Выводит список сообщений обратной связи для администратора
     *
     * @param int $current_page Текущая страница для пагинации.
     */
    public function actionIndex($current_page)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin) {
            // блок формирования страницы
            if ($current_page == 'all' || $current_page == 0) {
                $feedbacks = $this->feedbackModel->getAll();
                include_once('views/common/header.php');
                //include_once('views/common/sort.php');
                include_once('views/user/list.php');
                include_once('views/common/footer.php');
            } else {
                // pagination
                $cards_on_page = 6; // сколько карточек(строк) выводить на страницу
                $count = $this->feedbackModel->getAllCount(); // сколько всего карточек
                $path_to_page = 'feedbacks/index/'; // путь к странице - 'bouquets', 'orders', 'users' и тп
                $current_page = $this->paginationModel->getCurrentPage($current_page, $count,
                    $cards_on_page); // pagination
                $feedbacks = $this->feedbackModel->getAllWithLimit(($current_page - 1) * $cards_on_page,
                    $cards_on_page);
                include_once('views/common/header.php');
                //include_once('views/common/sort.php');
                include_once('views/feedback/list.php');
                $this->paginationModel->getPagination($current_page, $cards_on_page, $count,
                    $path_to_page); // pagination
                include_once('views/common/footer.php');
            }
        } else {
            header("Location: " . FULL_SITE_ROOT . 'error/404');
        }
    }

    /**
     * Формирует страницу с формой обратной связи
     */
    public function actionView()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $feedbacks = $this->feedbackModel->getAll();

        include_once('views/common/header.php');
        include_once('views/feedback/list.php');
        include_once('views/common/footer.php');
    }

    /**
     * Добавляет сообщение из формы обратной связи
     */
    public function actionAdd()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        $feedbackName = htmlentities($_POST['feedbackName']);
        $feedbackEmail = htmlentities($_POST['feedbackEmail']);
        $feedbackPhone = htmlentities($_POST['feedbackPhone']);
        $feedbackText = htmlentities($_POST['feedbackText']);

        $result = $this->feedbackModel->addFeedback(
            $feedbackName,
            $feedbackEmail,
            $feedbackPhone,
            $feedbackText
        );
        if ($result) {
            include_once('views/common/header.php');
            include_once('views/feedback/success.php');
            include_once('views/common/footer.php');
        } else {
            header('Location: ' . FULL_SITE_ROOT . 'errors/1');
        }
    }

    /**
     * Удаляет сообщение из базы сообщений обратной связи
     *
     * @param int $id Идентификатор сообщения обратной связи.
     */
    public function actionDelete($id)
    {
        $isAdmin = $this->userModel->checkIfAdmin();
        if ($isAdmin) {
            $this->feedbackModel->deleteFeedback($id);
            header("Location: " . FULL_SITE_ROOT . 'feedback');
        } else {
            header('Location: ' . FULL_SITE_ROOT . 'errors/404');
        }
    }
}