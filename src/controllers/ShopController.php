<?php

/**
 * Controller для передачи информации об имени магазина и т.п. в соответствующие View, а так же
 * для манипуляции данными - модификация, удаление, добавление.
 */
class ShopController
{

    private $userModel;
    private $shopModel;

    public function __construct()
    {
        $this->userModel = new User();
        $this->shopModel = new Shop();
    }

    /**
     * Выводит таблицу всех магазинов
     */
    public function actionList() {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        if ($isAdmin) {
            $shops = $this->shopModel->getAllShops();
            include_once('views/common/header.php');
            include_once('views/shop/list.php');
            include_once('views/common/footer.php');
        } else {
            header("Location:" . FULL_SITE_ROOT . 'error/404');
        }
    }

    /**
     * Формирует страницу редактирования магазина.
     *
     * @param int $id Идентификатор магазина для редактирования.
     */
    public function actionEdit($id)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin) {
            if (isset($_POST['shop_name'])) {

                $shop_for_edit = $this->shopModel->getShopByID($id);

                $shop_name = htmlentities($_POST['shop_name']);
                $shop_phone = htmlentities($_POST['shop_phone']);
                $shop_email = htmlentities($_POST['shop_email']);
                $shop_address = htmlentities($_POST['shop_address']);
                //$shop_is_active = htmlentities($_POST['shop_is_active']);
                isset($_POST['shop_is_active']) ? $shop_is_active = 1 : $shop_is_active = 0;

                $this->shopModel->editShop(
                    $id,
                    $shop_name,
                    $shop_phone,
                    $shop_email,
                    $shop_address,
                    $shop_is_active
                );
                header('Location: ' . FULL_SITE_ROOT . 'shops/list');
            } else {
                $shop_for_edit = $this->shopModel->getShopByID($id);
                include_once('views/common/header.php');
                include_once('views/shop/edit.php');
                include_once('views/common/footer.php');
            }
        } else {
            header("Location: " . FULL_SITE_ROOT . 'errors/404');
        }
    }

    /**
     * Удаляет конкретный магазин.
     *
     * @param int $id Идентификатор магазина.
     */
    public function actionDelete($id)
    {
        $this->shopModel->deleteShop($id);
        header("Location: " . FULL_SITE_ROOT . 'shops/list');
    }

}