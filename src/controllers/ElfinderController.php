<?php

/**
 * Controller для формирования страницы плагина ElFinder.
 */
class ElfinderController
{

    private $userModel;

    public function __construct()
    {
        $this->userModel = new User();
    }

    /**
     * Отображает страницу ElFinder
     */
    public function actionView()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin && $isAuthorized) {
            include_once('views/elfinder/header_elfinder.php');
            include_once('views/elfinder/view.php');
            include_once('views/common/footer.php');
        } else {
            header('Location: ' . FULL_SITE_ROOT . 'errors/404');
        }
    }
}

