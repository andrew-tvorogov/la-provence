<?php

/**
 * Controller корзины для отображения данных корзины, манипуляции с ними.
 */
class CartController
{

    private $bouquetModel;
    private $userModel;
    private $cartModel;
    private $deliveryModel;

    public function __construct()
    {
        $this->bouquetModel = new Bouquet();
        $this->userModel = new User();
        $this->cartModel = new Cart();
        $this->deliveryModel = new Delivery();
    }

    /**
     * Отображает информацию о корзине
     */
    public function actionIndex()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $bouquetsCart = $this->cartModel->bouquetsInCart();
        $deliveries = $this->deliveryModel->getDeliveriesList();
        //$bouquets = $this->bouquetModel->getAll();

        $total = 0;
        foreach ($bouquetsCart as $orderTotal) {
            $total = $total + ($orderTotal['bouquet_price'] * $orderTotal['bouquet_quantity']);
        }

        include_once('views/common/header.php');
        include_once('views/cart/list.php');
        include_once('views/common/footer.php');
    }

}