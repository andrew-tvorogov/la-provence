<?php

/**
 * Controller для добавления и удаления товара из избранного конкретного пользователя
 */
class FavoriteController
{
    private $favoriteModel;

    public function __construct()
    {
        $this->favoriteModel = new Favorite();
    }

    /**
     * Добавляет и удаляет товар из избранного
     *
     * @param int $bouquet_id Идентификатор букета.
     * @param int $user_id Идентификатор пользователя.
     */
    public function actionSwitch($bouquet_id, $user_id)
    {
        // если получили bouquet_id и user_id
        if ($this->favoriteModel->checkIfAlreadyInFavorites($bouquet_id, $user_id)) {
            // да, в favorites - значит удаляем из favorites
            $this->favoriteModel->favoriteDelete($bouquet_id, $user_id);
            // удалили, возвращаем 0 для xhr
            echo '0';
        } else {
            // нет, не в favorites - значит добавляем в favorites
            $this->favoriteModel->favoriteAdd($bouquet_id, $user_id);
            // добавили, возвращаем 1 для xhr
            echo '1';
        }
    }

}