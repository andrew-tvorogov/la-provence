<?php

/**
 * Controller для передачи информации о товарах в соответствующие View, а так же
 * для манипуляции данными - модификация, удаление, добавление.
 */
class BouquetController
{
    private $bouquetModel;
    private $userModel;
    private $cartModel;
    private $favoriteModel;
    private $floristModel;
    private $paginationModel;

    public function __construct()
    {
        $this->bouquetModel = new Bouquet();
        $this->userModel = new User();
        $this->cartModel = new Cart();
        $this->favoriteModel = new Favorite();
        $this->floristModel = new Florist();
        $this->paginationModel = new Pagination();
    }

    /**
     * Формирует страницу с карточками товаров.
     *
     * @param int $current_page Текущая страница.
     */
    public function actionIndex($current_page)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $bouquetsCart = $this->cartModel->bouquetsInCart();

        // блок формирования страницы
        if ($current_page == 'start') {
            $bouquets = $this->bouquetModel->getAll();
            include_once('views/common/header.php');
            include_once('views/banner/banner.php'); // при старте страницы выводим banner
            include_once('views/common/sort.php');
            include_once('views/bouquet/list.php');
            include_once('views/common/footer.php');
        } elseif ($current_page == 'all' || $current_page == 0) {
            $bouquets = $this->bouquetModel->getAll();
            include_once('views/common/header.php');
            include_once('views/common/sort.php');
            include_once('views/bouquet/list.php');
            include_once('views/common/footer.php');
        } else {
            //pagination
            $cards_on_page = 4; // сколько карточек(строк) выводить на страницу
            $count = $this->bouquetModel->getAllCount(); // сколько всего карточек существует
            $path_to_page = 'bouquets/index/'; // путь к странице - 'bouquets', 'orders', 'users' и тп
            $current_page = $this->paginationModel->getCurrentPage($current_page, $count, $cards_on_page); // pagination
            $bouquets = $this->bouquetModel->getAllWithLimit(($current_page - 1) * $cards_on_page, $cards_on_page);
            include_once('views/common/header.php');
            include_once('views/common/sort.php');
            include_once('views/bouquet/list.php');
            $this->paginationModel->getPagination($current_page, $cards_on_page, $count, $path_to_page); // pagination
            include_once('views/common/footer.php');
        }
    }

    /**
     * Формирует страницу с карточками товаров для администратора.
     *
     * @param int $current_page Текущая страница.
     */
    public function actionList($current_page)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $bouquetsCart = $this->cartModel->bouquetsInCart();

        // блок формирования страницы
        if ($current_page == 'all' || $current_page == 0) {
            $bouquets = $this->bouquetModel->getAllWithDeleted();
            include_once('views/common/header.php');
            include_once('views/bouquet/list_admin.php');
            include_once('views/common/footer.php');
        } else {
            //pagination
            $cards_on_page = 4; // сколько карточек(строк) выводить на страницу
            $count = $this->bouquetModel->getAllCountWithDeleted(); // сколько всего карточек существует
            $path_to_page = 'bouquets/list/'; // путь к странице - 'bouquets', 'orders', 'users' и тп
            $current_page = $this->paginationModel->getCurrentPage($current_page, $count, $cards_on_page); // pagination
            $bouquets = $this->bouquetModel->getAllWithLimitWithDeleted(($current_page - 1) * $cards_on_page,
                $cards_on_page);
            include_once('views/common/header.php');
            include_once('views/bouquet/list_admin.php');
            $this->paginationModel->getPagination($current_page, $cards_on_page, $count, $path_to_page); // pagination
            include_once('views/common/footer.php');
        }
    }

    /**
     * Формирует страницу с карточками товаров для добавления,
     * взаимодействует с model - добавление товара.
     */
    public function actionAdd()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        $images = $this->bouquetModel->getBouquetImages();
        $florists = $this->floristModel->getAllFlorists();
        $colors = $this->bouquetModel->getBouquetColors();
        $sizes = $this->bouquetModel->getBouquetSizes();
        $categories = $this->bouquetModel->getBouquetCategories();

        if ($isAdmin) {
            // проверяем пришли ли данные формы
            if (isset($_POST['name'])) {
                $bouquet_name = htmlentities($_POST['name']);
                $bouquet_price = htmlentities($_POST['price']);
                $bouquet_image = htmlentities($_POST['image']);
                $bouquet_description = htmlentities($_POST['description']);
                $bouquet_florist_id = htmlentities($_POST['florist_id']);
                $bouquet_color_id = htmlentities($_POST['color_id']);
                $bouquet_size_id = htmlentities($_POST['size_id']);
                $bouquet_category_id = htmlentities($_POST['category_id']);

                $this->bouquetModel->addBouquet(
                    $bouquet_name,
                    $bouquet_price,
                    $bouquet_image,
                    $bouquet_description,
                    $bouquet_florist_id,
                    $bouquet_color_id,
                    $bouquet_size_id,
                    $bouquet_category_id
                );
                header('Location: ' . FULL_SITE_ROOT . 'bouquets/list/0');
            } else {
                include_once('views/common/header.php');
                include_once('views/bouquet/add.php');
                include_once('views/common/footer.php');
            }
        } else {
            // не admin - просто идём на главную
            header("Location: " . FULL_SITE_ROOT);
        }
    }

    /**
     * Формирует страницу с карточками товаров для редактирования,
     * взаимодействует с model - редактирование товара.
     *
     * @param int $id Идентификатор страницы для редактирования.
     */
    public function actionEdit($id)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        $images = $this->bouquetModel->getBouquetImages();
        $florists = $this->floristModel->getAllFlorists();
        $colors = $this->bouquetModel->getBouquetColors();
        $sizes = $this->bouquetModel->getBouquetSizes();
        $categories = $this->bouquetModel->getBouquetCategories();

        if ($isAdmin) {

            if (isset($_POST['new_name'])) {
                $id = htmlentities($_POST['new_id']);
                $bouquet_new_name = htmlentities($_POST['new_name']);
                $bouquet_new_price = htmlentities($_POST['new_price']);
                $bouquet_new_image = htmlentities($_POST['new_image']);
                $bouquet_new_description = htmlentities($_POST['new_description']);
                $bouquet_new_florist_id = htmlentities($_POST['new_florist_id']);
                $bouquet_new_color_id = htmlentities($_POST['new_color_id']);
                $bouquet_new_size_id = htmlentities($_POST['new_size_id']);
                $bouquet_new_category_id = htmlentities($_POST['new_category_id']);

                $this->bouquetModel->editBouquet(
                    $bouquet_new_name,
                    $bouquet_new_price,
                    $bouquet_new_image,
                    $bouquet_new_description,
                    $bouquet_new_florist_id,
                    $bouquet_new_color_id,
                    $bouquet_new_size_id,
                    $bouquet_new_category_id,
                    $id
                );
                header('Location: ' . FULL_SITE_ROOT . 'bouquet/view/' . $id);
            } else {
                $bouquetForEdit = $this->bouquetModel->getBouquetByID($id);

                include_once('views/common/header.php');
                include_once('views/bouquet/edit.php');
                include_once('views/common/footer.php');
            }
        } else {
            // не admin - просто идём на главную
            header("Location: " . FULL_SITE_ROOT);
        }
    }

    /**
     * Формирует страницу конкретного товара.
     *
     * @param int $id Идентификатор товара.
     */
    public function actionView($id)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $bouquetsCart = $this->cartModel->bouquetsInCart();

        $bouquet = $this->bouquetModel->getBouquetByIDFullInfo($id);
        if (empty($bouquet)) {
            header('Location: ' . FULL_SITE_ROOT . 'errors/2');
        }
        $title = "Bouquet - View";
        $bouquetForView = $this->bouquetModel->getBouquetByID($id);
        include_once('views/common/header.php');
        include_once('views/bouquet/view.php');
        include_once('views/common/footer.php');
        return true;
    }

    /**
     * Удаляет конкретный товар.
     *
     * @param int $id Идентификатор товара.
     */
    public function actionDelete($id)
    {
        $this->bouquetModel->deleteBouquet($id);
        header('Location: ' . FULL_SITE_ROOT . 'bouquets/list/0');
    }

}