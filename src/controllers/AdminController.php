<?php

/**
 * Controller для передачи информации о товарах, флористах, букетах в соответствующие View.
 */
class AdminController
{

    private $bouquetModel;
    private $floristModel;
    private $userModel;
    private $favoriteModel;
    private $orderModel;
    private $shopModel;

    public function __construct()
    {

        $this->bouquetModel = new Bouquet();
        $this->floristModel = new Florist();
        $this->userModel = new User();
        $this->favoriteModel = new Favorite();
        $this->orderModel = new Order();
        $this->shopModel = new Shop();
    }

    /**
     * Формирует страницу с таблицами пользователей, товаров, сотрудников.
     */
    public function actionView()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin) {
            $bouquets = $this->bouquetModel->getAll();
            $florists = $this->floristModel->getAllFlorists();

            $bouquets_count = $this->bouquetModel->getAllCount();
            $users_count = $this->userModel->getAllCount();
            $orders_count = $this->orderModel->getAllCount();
            $favorites_count = $this->favoriteModel->getAllCount();
            $shops = $this->shopModel->getAllShops();

            include_once('views/common/header.php');
            include_once('views/admin/view.php');
            include_once('views/common/footer.php');
        } else {
            header("Location:" . FULL_SITE_ROOT . 'error/404');
        }
    }
}