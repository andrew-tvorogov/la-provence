<?php


/**
 * Controller для управления баннером,
 * для манипуляции данными - модификация, удаление, добавление.
 */
class BannerController
{
    private $userModel;
    private $bannerModel;

    public function __construct()
    {
        $this->userModel = new User();
        $this->bannerModel = new Banner();
    }

    /**
     * Включает и выключает баннер
     *
     * @param int $id Идентификатор конкретного баннера.
     */
    public function actionEnable($id)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        if ($isAdmin) {
            $this->bannerModel->enableBanner($id);
            header('Location: ' . FULL_SITE_ROOT . 'banners/list');
        }
    }

    /**
     * Выводит таблицу всех баннеров
     */
    public function actionList() {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        if ($isAdmin) {
            $banners = $this->bannerModel->getAllBanners();
            include_once('views/common/header.php');
            include_once ('views/banner/list.php');
            include_once('views/common/footer.php');
        } else {
            header("Location: " . FULL_SITE_ROOT . "errors/404");
        }
    }

    /**
     * Добавляет новый баннер
     */
    public function actionAdd()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $bannerMediaFiles = $this->bannerModel->getBannerFiles(); // по-умолчанию получает все картинки

        if ($isAdmin) {
            if (isset($_POST['banner_img'])) {
                //TODO: исключить SQL инъекции!!!
                $banner_img = htmlentities($_POST['banner_img']);
                $banner_video = htmlentities($_POST['banner_video']);
                $banner_video_poster = htmlentities($_POST['banner_video_poster']);
                $banner_main_text = $_POST['banner_main_text'];
                $banner_small_text = $_POST['banner_small_text'];
                $banner_small_text_logo = $_POST['banner_small_text_logo'];
                $banner_button_text = $_POST['banner_button_text'];
                $this->bannerModel->addBanner(
                    $banner_img,
                    $banner_video,
                    $banner_video_poster,
                    $banner_main_text,
                    $banner_small_text,
                    $banner_small_text_logo,
                    $banner_button_text
                );
                header('Location: ' . FULL_SITE_ROOT . 'banners/list');
            } else {
                include_once('views/common/header.php');
                include_once('views/banner/add.php');
                include_once('views/common/footer.php');
            }
        } else {
            // не admin - ошибка 404
            header("Location: " . FULL_SITE_ROOT . "errors/404");
        }
    }

    /**
     * Формирует страницу редактирования баннера.
     *
     * @param int $id Идентификатор баннера для редактирования.
     */
    public function actionEdit($id)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $banner_for_edit = $this->bannerModel->getBannerById($id);
        $bannerMediaFiles = $this->bannerModel->getBannerFiles(); // по-умолчанию получает все картинки

        if ($isAdmin) {
            if (empty($banner_for_edit)) header('Location: ' . FULL_SITE_ROOT . 'errors/4');
            if (isset($_POST['banner_img'])) {
                $banner_id = $id;
                $banner_img = htmlentities($_POST['banner_img']);
                $banner_video = htmlentities($_POST['banner_video']);
                $banner_video_poster = htmlentities($_POST['banner_video_poster']);
                $banner_main_text = $_POST['banner_main_text'];
                $banner_small_text = $_POST['banner_small_text'];
                $banner_small_text_logo = $_POST['banner_small_text_logo'];
                $banner_button_text = $_POST['banner_button_text'];

                $this->bannerModel->editBanner(
                    $banner_id,
                    $banner_img,
                    $banner_video,
                    $banner_video_poster,
                    $banner_main_text,
                    $banner_small_text,
                    $banner_small_text_logo,
                    $banner_button_text
                );
                header('Location: ' . FULL_SITE_ROOT . 'banners');
            } else {
                if (empty($banner_for_edit)) header('Location: ' . FULL_SITE_ROOT . 'errors/4');
                include_once('views/common/header.php');
                include_once('views/banner/edit.php');
                include_once('views/common/footer.php');
            }
        } else {
            // не admin - ошибка 404
            header("Location: " . FULL_SITE_ROOT . "errors/404");
        }
    }

    /**
     * Удаляет конкретный баннер.
     *
     * @param int $id Идентификатор баннера.
     */
    public function actionDelete($id)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        if ($isAdmin) {
            $this->bannerModel->deleteBanner($id);
            header("Location: " . FULL_SITE_ROOT . "banners");
        } else {
            header("Location: " . FULL_SITE_ROOT . "errors/404");
        }
    }

}