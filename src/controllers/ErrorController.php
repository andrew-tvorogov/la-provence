<?php

/**
 * Controller для формирования страниц с информацией об ошибках.
 */
class ErrorController
{

    private $userModel;

    public function __construct()
    {
        $this->userModel = new User();
    }

    /**
     * Отображает страницу ошибки
     *
     * @param int $errorNumber Номер ошибки.
     */
    public function actionIndex($errorNumber)
    {
        $userModel = new User();
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $error = $errorNumber;

        include_once('views/common/header.php');
        include_once('views/error/index.php');
        include_once('views/common/footer.php');
    }
}