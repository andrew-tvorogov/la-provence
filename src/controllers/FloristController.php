<?php

/**
 * Controller для управления флористами,
 * для манипуляции данными - модификация, удаление.
 */
class FloristController
{
    private $floristModel;
    private $userModel;

    public function __construct()
    {
        $this->floristModel = new Florist();
        $this->userModel = new User();
    }

    /**
     * Выводит страницу с карточками всех флористов
     */
    public function actionAll(){
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $florists = $this->floristModel->getAllFlorists();
        include_once('views/common/header.php');
        include_once('views/florist/list.php');
        include_once('views/common/footer.php');
        return true;
    }

    /**
     * Выводит информацию по флористу,
     * его букеты
     *
     * @param int $id Идентификатор флориста.
     */
    public function actionView($id){
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $selectedFlorist = $this->floristModel->getFlorist($id);
        $floristsBouquets = $this->floristModel->getFloristBouquets($id);

        if (empty($selectedFlorist)) header('Location: ' . FULL_SITE_ROOT . 'errors/3');

        include_once('views/common/header.php');
        include_once('views/florist/view.php');
        include_once('views/common/footer.php');
        return true;
    }

    /**
     * Выводит страницу редактирования флориста
     *
     * @param int $id Идентификатор флориста.
     */
    public function actionEdit($id) {

        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin){
            $florist_for_edit = $this->floristModel->getFlorist($id);
            $floristsMediaFiles = $this->floristModel->getFloristsFiles();

            if ( isset($_POST['florist_name']) ) {
                $florist_name = $_POST['florist_name'];
                $florist_surname = $_POST['florist_surname'];
                $florist_phone = $_POST['florist_phone'];
                $florist_img = $_POST['florist_img'];
                $florist_descr = $_POST['florist_descr'];

                $this->floristModel->editFlorist(
                    $id,
                    $florist_name,
                    $florist_surname,
                    $florist_phone,
                    $florist_img,
                    $florist_descr
                    );
                header("Location: " . FULL_SITE_ROOT . "employees/list");
            } else {
                include_once('views/common/header.php');
                include_once ('views/florist/edit.php');
                include_once('views/common/footer.php');
            }
        } else {
            header("Location: " . FULL_SITE_ROOT . "errors/404");
        }
    }

}