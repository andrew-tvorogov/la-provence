<?php

/**
 * Controller для управления информационными страницами со статическим контентом,
 * для манипуляции данными - модификация, удаление, добавление.
 */
class InfoController
{
    private $userModel;
    private $infoModel;

    public function __construct()
    {
        $this->userModel = new User();
        $this->infoModel = new Info();
    }

    /**
     * Отображает страницу
     *
     * @param int $content_path Путь до страницы со статическим контентом.
     */
    public function actionView($content_path)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $pageContent = $this->infoModel->getPageContent($content_path);
        if($pageContent){
            include_once('views/common/header.php');
            include_once('views/info/view.php');
            include_once('views/common/footer.php');
        } else {
            header("Location: " . FULL_SITE_ROOT . 'errors/404');
        }
    }

    /**
     * Отображает страницу редактирования страницы со статическим контентом.
     *
     * @param int $id Идентификатор страницы со статическим контентом.
     */
    public function actionEdit($id)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin) {
            if (isset($_POST['new_text'])) {
                $pageForEdit = $this->infoModel->getPageByID($id);
                $id = htmlentities($_POST['new_id']);
                $new_header = htmlentities($_POST['new_header']);
                //$new_text = htmlentities($_POST['new_text']);
                $new_text = $_POST['new_text'];
                $this->infoModel->editPage(
                    $id,
                    $new_header,
                    $new_text
                );
                header('Location: ' . FULL_SITE_ROOT . 'info/' . $pageForEdit['content_path']);
            } else {
                $pageForEdit = $this->infoModel->getPageByID($id);
                include_once('views/common/header.php');
                include_once('views/info/edit.php');
                include_once('views/common/footer.php');
            }
        } else {
            header("Location: " . FULL_SITE_ROOT . 'errors/404');
        }
    }

    /**
     * Отображает список страниц со статическим контентом.
     */
    public function actionList() {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin) {
            $pages = $this->infoModel->getPages();
            include_once('views/common/header.php');
            include_once('views/info/list.php');
            include_once('views/common/footer.php');
        } else {
            header("Location:" . FULL_SITE_ROOT . 'error/404');
        }
    }

    /* TODO: добавление страницы
    public function actionAdd()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $contentModules = $this->infoModel->getContentModules();
        $contentSections = $this->infoModel->getContentSections();
        if(){
            include_once('views/common/header.php');
            include_once('views/info/add.php');
            include_once('views/common/footer.php');
        } else {
            header("Location: " . FULL_SITE_ROOT . 'errors/404');
        }
    }
    */

}