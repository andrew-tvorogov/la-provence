<?php

/**
 * Controller для отображения заказов, а так же
 * для манипуляции данными - модификация, удаление, добавление.
 */
class OrderController
{
    private $bouquetModel;
    private $floristModel;
    private $userModel;
    private $cartModel;
    private $deliveryModel;
    private $orderModel;
    private $paginationModel;

    public function __construct()
    {
        $this->bouquetModel = new Bouquet();
        $this->floristModel = new Florist();
        $this->userModel = new User();
        $this->cartModel = new Cart();
        $this->deliveryModel = new Delivery();
        $this->orderModel = new Order();
        $this->paginationModel = new Pagination();
    }

    /**
     * Формирует страницу с таблицей заказов.
     *
     * @param int $current_page Текущая страница.
     */
    public function actionIndex($current_page)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin) {
            // блок формирования страницы
            if ($current_page == 'all' || $current_page == 0) {
                $orders = $this->orderModel->getOrders();
                include_once('views/common/header.php');
                include_once('views/order/list.php');
                include_once('views/common/footer.php');
            } else {
                // pagination
                $cards_on_page = 6; // сколько карточек(строк) выводить на страницу
                $count = $this->orderModel->getAllCount(); // сколько всего карточек
                $path_to_page = 'orders/index/'; // путь к странице - 'bouquets', 'orders', 'users' и тп
                $current_page = $this->paginationModel->getCurrentPage($current_page, $count,
                    $cards_on_page); // pagination
                $orders = $this->orderModel->getAllWithLimit(($current_page - 1) * $cards_on_page, $cards_on_page);
                include_once('views/common/header.php');
                include_once('views/order/list.php');
                $this->paginationModel->getPagination($current_page, $cards_on_page, $count,
                    $path_to_page); // pagination
                include_once('views/common/footer.php');
            }
        } else {
            header("Location: " . FULL_SITE_ROOT . 'error/404');
        }
    }

    /**
     * Формирует и добавляет заказ.
     */
    public function actionAdd()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        //$florists = $this->floristModel->getAllFlorists();

        if (isset($_POST['user_id'])) { //если данные формы пришли
            // получить данные из формы
            $user_id = $_POST['user_id'];
            if (!$user_id) {
                $user_id = 1; // anonymouse id 1
            }
            $delivery_id = $_POST['delivery_type_id'];
            $user_name = htmlentities($_POST['user_name']);
            $user_phone = htmlentities($_POST['user_phone']);
            $delivery_address = htmlentities($_POST['delivery_address']);
            $delivery_date = htmlentities($_POST['delivery_date']);
            $card_message = htmlentities($_POST['card_message']);
            $comment = htmlentities($_POST['comment']);

            // корзина из куки
            $bouquetsCart = [];
            if (isset($_COOKIE['cart'])) {
                $cart = json_decode($_COOKIE['cart'], true);
                $ids = array_keys($cart);
                $bouquetsCartInfo = $this->cartModel->getCartInfo($ids);
                foreach ($bouquetsCartInfo as &$bouquetCart) {
                    $bouquetCart['bouquet_quantity'] = $cart[$bouquetCart['bouquet_id']];
                }
                $bouquetsCart = $bouquetsCartInfo;
            }
            $order_id = $this->orderModel->createOrder(
                $delivery_id,
                $user_id,
                $delivery_address,
                $comment,
                $user_name,
                $user_phone,
                $delivery_date,
                $card_message,
                $bouquetsCart);

            // чистим корзину
            if ($user_id != 1) {
                setcookie("cart", '', (time() - 3600), '/'); // удаление куков корзины
                //header("Location: cabinet.php");
            } else {
                setcookie("cart", '', (time() - 3600), '/'); // удаление куков корзины
                //header("Location: index.php");
            }

            // рисуем сообщение о добавлении заказа
            include_once('views/common/header.php');
            include_once('views/order/message.php');
            include_once('views/common/footer.php');
            return true;
        }
        //error - ничего не передано в из формы
        header("Location: " . FULL_SITE_ROOT . "errors/404");
    }

    /**
     * Просмотр заказа
     *
     * @param int $id Идентификатор заказа.
     */
    public function actionView($id)
    {
        //TODO: сделать просмотр деталей заказа!
    }

    /**
     * Редактирование заказа
     */
    public function actionEdit()
    {
        //TODO: сделать редактирование заказа!
    }

    /**
     * Удаление заказа
     *
     * @param int $id Идентификатор заказа.
     */
    public function actionDelete($id)
    {
        $order_id = $id;
        $this->orderModel->deleteOrder($order_id);
        header("Location: " . FULL_SITE_ROOT . 'orders/index/1');
        return true;
    }

}