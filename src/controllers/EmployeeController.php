<?php

/**
 * Controller для формирования страницы со списком сотрудников.
 */
class EmployeeController
{

    private $userModel;
    private $floristModel;
    private $deliveryModel;
    // другие отделы

    public function __construct()
    {
        $this->floristModel = new Florist();
        $this->userModel = new User();
        $this->deliveryModel = new Delivery();
    }

    /**
     * Отображает список сотрудников
     */
    public function actionList() {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        if ($isAdmin) {
            $florists = $this->floristModel->getAllFlorists();
            include_once('views/common/header.php');
            include_once('views/florist/list_admin.php');
            include_once('views/common/footer.php');
        } else {
            header("Location:" . FULL_SITE_ROOT . 'error/404');
        }
    }

}