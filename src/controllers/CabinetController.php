<?php

/**
 * Controller личного кабинета для отображения информации о пользователе,
 * его заказах, избранном, личных данных.
 */
class CabinetController
{
    private $floristModel;
    private $userModel;
    private $orderModel;
    private $favoriteModel;
    private $cabinetModel;

    public function __construct()
    {
        $this->floristModel = new Florist();
        $this->userModel = new User();
        $this->orderModel = new Order();
        $this->favoriteModel = new Favorite();
        $this->cabinetModel = new Cabinet();
    }

    /**
     * Отображает информацию о пользователе
     */
    public function actionView(){
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        $userId = $_COOKIE['user'];

        $user = $this->userModel->getUserInfo($userId); // функция в users
        $orders = $this->userModel->getUserOrders($userId); // функция в users.php
        $favorites = $this->favoriteModel->getFavorites($userId); // функция в favorites_add_remove.php

        if (isset($_POST['user_id']) && isset($_POST['phone']) && isset($_POST['name'])) {
            $uId = $_POST['user_id'];
            $uPhone = $_POST['phone'];
            $uName = $_POST['name'];
            $this->cabinetModel->userUpdate($uName, $uPhone, $uId);
            setcookie('user_name', $uName, time() + 2*24*3600, '/');
            header('Location: ' . FULL_SITE_ROOT . 'cabinet');
        }

        include_once('views/common/header.php');
        include_once('views/cabinet/view.php');
        include_once('views/common/footer.php');
        return true;
    }

}