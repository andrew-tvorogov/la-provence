<?php

/**
 * Controller для управления специальными предложениями,
 * для манипуляции данными - модификация, удаление, добавление.
 */
class OfferController
{
    private $userModel;
    private $offerModel;

    public function __construct()
    {
        $this->userModel = new User();
        $this->offerModel = new Offer();
    }

    /**
     * Формирует страницу со всеми активными предложениями для пользователя.
     */
    public function actionIndex()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $offers = $this->offerModel->getActiveOffers();

        include_once('views/common/header.php');
        if ($isAdmin) {
            // все предложения для admin'a
            $offers = $this->offerModel->getAllOffers();
        }
        include_once('views/offer/list.php');
        include_once('views/common/footer.php');
    }

    /**
     * Формирует страницу для добавления специального предложения.
     */
    public function actionAdd()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if ($isAdmin) {
            $offerMediaFiles = $this->offerModel->getOfferFiles(); // по-умолчанию получает все картинки
            // проверяем пришли ли данные формы
            if (isset($_POST['offer_img'])) {
                $offer_img = $_POST['offer_img'];
                //$offer_is_active = $_POST['offer_is_active'];
                isset($_POST['offer_is_active']) ? $offer_is_active = 1 : $offer_is_active = 0;
                $this->offerModel->addOffer(
                    $offer_img,
                    $offer_is_active
                );
                header('Location: ' . FULL_SITE_ROOT . 'offers');
            } else {
                include_once('views/common/header.php');
                include_once('views/offer/add.php');
                include_once('views/common/footer.php');
            }
        } else {
            // не admin - ошибка 404
            header("Location: " . FULL_SITE_ROOT . "errors/404");
        }
    }

    /**
     * Формирует страницу для редактирования специального предложения.
     *
     * @param int $id Идентификатор специального предложения.
     */
    public function actionEdit($id)
    {

        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        $offerMediaFiles = $this->offerModel->getOfferFiles(); // по-умолчанию получает все картинки

        if ($isAdmin) {
            $offer_for_edit = $this->offerModel->getOfferById($id);
            if (isset($_POST['offer_img'])) {
                $offer_img = $_POST['offer_img'];
                //$offer_is_active = $_POST['offer_is_active'];
                isset($_POST['offer_is_active']) ? $offer_is_active = 1 : $offer_is_active = 0;
                $this->offerModel->editOffer(
                    $id,
                    $offer_img,
                    $offer_is_active);
                header("Location: " . FULL_SITE_ROOT . "offers");
            } else {
                include_once('views/common/header.php');
                include_once('views/offer/edit.php');
                include_once('views/common/footer.php');
            }
        } else {
            header("Location: " . FULL_SITE_ROOT . "errors/404");
        }
    }

    /**
     * ФУдаляет специальное предложение.
     *
     * @param int $id Идентификатор специального предложения.
     */
    public function actionDelete($id)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();
        if ($isAdmin) {
            $this->offerModel->deleteOffer($id);
            header("Location: " . FULL_SITE_ROOT . "offers");
        } else {
            header("Location: " . FULL_SITE_ROOT . "errors/404");
        }
    }
}